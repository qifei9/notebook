# Github & Gitlab

## Automatic issue closing

Include key words in the commit message or merge request description. For example:

```
Awesome commit message

Fix #20, Fixes #21 and Closes group/otherproject#22.
This commit is also related to #17 and fixes #18, #19
and https://gitlab.example.com/group/otherproject/issues/23.
```

will close `#18`, `#19`, `#20`, and `#21` in the project this commit is pushed to, as well as `#22` and `#23` in group/otherproject. `#17` *won’t be closed* as it does not match the pattern. It works with multi-line commit messages as well as one-liners when used with `git commit -m`.[^1]

---

## References

[^1] https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html
