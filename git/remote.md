# Remote

## Remove a remote

[^1]

``` bash
git remote rm <remote-name>
```

## Rename a remote

``` bash
git remote rename <old name> <new name>
```

## Change the url

[^2]

``` bash
git remote set-url origin <new url>
```

## References

[^1] https://help.github.com/articles/removing-a-remote/

[^2] https://stackoverflow.com/questions/16330404/how-to-remove-remote-origin-from-git-repo
