# Ribozyme

## Large ribozyme

- Group I intron
- Group II intron
- RNase P

## Small self-cleaving ribozyme

- Hammerhead ribozyme

    - include type I, II and III and an unusual variants

    - cleavage site *could* be close to 5' end

- HDV and HDV-like ribozyme

    - cleavage site is close to 5' end

- Hairpin ribozyme

    - cleavage site *could* be close to 5' end
    - Has ligation activity.

    - Metal ion
        - active in $$Co(NH_{3})_{6}^{3+}$$. $$Co^{3+}$$ binds $$NH_{3}$$ so tightly in solution that $$NH_{3}$$ does not dissociate to any appreciable extent, and therefore does not become protonated. This suggests there is no metal-catalyzed proton transfer or direct coordination to the RNA, but instead metals are only required for folding. (From wikipedia)

- glmS ribozyme

    - cleavage site is near 5' end
    - a ligand (GlcN6P) is requisite for fast cleavage

    - Kinetics
        - $$K_{obs} = 0.003 min^{-1}$$ under certain conditions.
        - $$K_{obs} = 3 min^{-1}$$ with GlcN6P.

- Varkud satelite (VS) ribozyme

    - The largest nucleolytic ribozyme (~150 nucleotides).
    - cleavage site is near 5' end.
    - The A730 loop and A756 base is critical.
    - The length (but not sequence) of helices III and V are important.
    - The base bulges in helices II and VI must be present for full activity.
    - Has ligation activity.

- Twister ribozyme

    - include type P1, P3 and P5

    - Many thousands of examples available in bacteria and eukaryotes.

    - cleavage site
        - Type P1: close to 5' end
        - Type P3: close to 3' end
        - Type P5: in the middle, a little bit close to 5' end

    - $$K_{obs}$$
        - 10 $$min^{-1}$$, highest observed value for a constructed bimolecular at 0.05 mM $$Mg^{2+}$$ pH 7.25.
        - ~1000 $$min^{-1}$$, inferred value according to the dependences of $$K_{obs}$$ on $$Mg^{2+}$$ and pH (pH 7.4, 1 mM $$Mg^{2+}$$).

    - Metal ion
        - active in many divalent metal ions: Mg, Mn, Ca, Co, Ni, Cd, Sr and Zn.
        - active in high concentration (1 M) of monovalent metal ions: Li, Na, K, Rb, Cs.
        - active in $$Co(NH_{3})_{6}^{3+}$$.

    - ligation: prolonged incubation of individual cleaved fragments could observed the ligation products.

- Twister sister ribozyme

    - cleavage site is near the 3' end

    - $$K_{obs}$$
        - ~5 $$min^{-1}$$, TS-1 at optimal condition.
        - more than 100 $$min^{-1}$$, TS-3 inferred.

    - Metal ion
        - active in many divalent metal ions: Mg, Mn, Ca, Co, Ni, Cd, Sr and Zn.
        - active in high concentration (1 M) of Li.
        - No activity in 1 M of Na, K, Rb, Cs.
        - No activity in $$Co(NH_{3})_{6}^{3+}$$.

    - Is it possible to have a permutation type P5?

- Pistol ribozyme

    - cleavage site is near 3' end

- Hatchet ribozyme

    - cleavage site is near 5' end

- Vg1 ribozyme

    - the smallest catalytic RNA, requires only a complex between `GAAA` and `UUU` to effect site-specific cleavage.

    - Metal ion
        - active in Mn, Cd and Cu (weak).
        - No activity in Mg, Ca, Ni and Co.
        - Zn triggers nonspecific degradation.

- CoTC ribozyme

    - Cleavage site is near 5' end.
    - GTP is a requisite for fast cleavage.
