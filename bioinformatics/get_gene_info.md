# Quary gene info

## Use AnnotationHub

``` R
library('AnnotationHub')

ah <- AnnotationHub()

query(ah, c("EnsDb", "Homo sapiens", "94"))

# Ensembl release 94, Homo sapiens ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{{{

ens_94_hs_gtf <- ah[['AH64923']]

ens_94_hs_gr <- genes(ens_94_hs_gtf)

ens_94_hs_ginfo_bed <- data.frame(
    Geneid = mcols(ens_94_hs_gr)$gene_id,
    chrom = seqnames(ens_94_hs_gr),
    start = start(ens_94_hs_gr) - 1,
    end = end(ens_94_hs_gr),
    strand = strand(ens_94_hs_gr),
    name = mcols(ens_94_hs_gr)$gene_name,
    type = mcols(ens_94_hs_gr)$gene_biotype,
    description = mcols(ens_94_hs_gr)$description
)

write.table(ens_94_hs_ginfo_bed,
            file = "../../omics/GRCh38/Ensembl_94_Hsapiens_gene_info.bed",
            quote = F, sep = "\t", row.names = F)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}}}
```

## Use biomaRt

``` R
library('biomaRt')

listMarts()

ensembl <- useMart("ensembl",dataset="hsapiens_gene_ensembl")

filters <- listFilters(ensembl)

attributes <- listAttributes(ensembl)

gi <- c('ENSG00000173114')

g <- getBM(attributes=c("hgnc_symbol","description","chromosome_name","strand","start_position","end_position","ensembl_gene_id", 'gene_biotype'), 
      filters = 'ensembl_gene_id', 
      values = gi,
      mart = ensembl)
```

Note, currently `getGene()` in biomaRt is broken (2019-03-11).[^1]

## References

[^1] https://support.bioconductor.org/p/74761/
