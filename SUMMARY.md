# Summary

* [Introduction](README.md)

## Bioconductor

- [GRanges](bioconductor/GRanges.md)
- [Mirrors](bioconductor/mirrors.md)

## Bioinformatics

- [Convert chromsome name](bioinformatics/convert_chrom_name.md)
- [Get gene info](bioinformatics/get_gene_info.md)
- [Hill equation and Hill plot](bioinformatics/hill.md)
- [Split seq file](bioinformatics/split_seq_file.md)
- [Taxonomy](bioinformatics/taxonomy.md)

## Biology

- [Ribozyme](biology/ribozyme.md)

## Docker

- [Images](docker/images.md)
- [Mirrors](docker/mirrors.md)
- [Repositories](docker/repositories.md)
- [Dockerfile examples](docker/dockerfile_examples.md)
- [Proxy](docker/proxy.md)

## English

- [Synonym](english/synonym.md)

## Git

- [checkout](git/checkout.md)
- [Delete a branch](git/delete_a_branch.md)
- [Delete a commit](git/delete_a_commit.md)
- [Diff](git/diff.md)
- [EOL](git/eol.md)
- [Github and Gitlab](git/github_gitlab.md)
- [Git LFS](git/git_lfs.md)
- [Rebase](git/rebase.md)
- [Remote](git/remote.md)
- [Remove big file](git/remove_big_file.md)
- [Remove sensitive data](git/remove_sensitive_data.md)
- [submodules](git/submodules.md)

## Linux

- [awk](linux/awk.md)
- [Bluetooth](linux/bluetooth.md)
- [Convert](linux/convert.md)
- [DNS](linux/dns.md)
- [ibus](linux/ibus.md)
- [merge gz files](linux/merge_gz_files.md)
- [Network](linux/network.md)
- [sed](linux/sed.md)
- [shell](linux/shell.md)
- [ubuntu](linux/ubuntu.md)
- [wallpaper](linux/wallpaper.md)
- [wget](linux/wget.md)
- [tips](linux/tips.md)

## MOOC

- [Learning How to Learn](mooc/learning_how_to_learn.md)

## Neovim

- [Fold](neovim/fold.md)
- [Lint and fix](neovim/lint_and_fix.md)
- [Moving around](neovim/moving_around.md)
- [Plugins](neovim/plugins.md)
- [regex](neovim/regex.md)
- [REPL](neovim/repl.md)
- [Syntax highlight](neovim/syntax.md)
- [Tips](neovim/tips.md)
- [Unicode](neovim/unicode.md)

## Python

- [argparse](python/argparse.md)
- [Biopython](python/biopython.md)
- [Conda](python/conda.md)
- [Conditional operator](python/conditional_operator.md)
- [Errors](python/errors.md)
- [Pandas](python/pandas.md)
- [Path](python/path.md)
- [Read .mat file](python/mat.md)
- [regex](python/regex.md)
- [requirements.txt](python/requirements.md)
- [Set](python/set.md)
- [Syntax](python/syntax.md)
- [Tips](python/tips.md)

## R

- [3D plot](r/3d_plot.md)
- [apply](r/apply.md)
- [Dataframe and tibble](r/dataframe_and_tibble.md)
- [External command](r/external_command.md)
- [Factor](r/factor.md)
- [File and Path](r/file_and_path.md)
- [Function](r/function.md)
- [ggplot2](r/ggplot2.md)
- [Join](r/join.md)
- [List](r/list.md)
- [Mirrors](r/mirrors.md)
- [NA](r/na.md)
- [palette](r/palette.md)
- [Permutation](r/permutation.md)
- [Plot](r/plot.md)
- [Read .mat file](r/mat.md)
- [readr](r/readr.md)
- [Regression](r/regression.md)
- [Replace values](r/replace_values.md)
- [Save](r/save.md)
- [Sort](r/sort.md)
- [stringr](r/stringr.md)
- [Tips](r/tips.md)
- [unique](r/unique.md)
- [Valid names](r/valid_names.md)

## Raspberry Pi

- [Optimize](raspberry_pi/optimize.md)
- [Setup](raspberry_pi/setup.md)
- [TV-box](raspberry_pi/tv-box.md)

## Statistics

- [odds ratio](statistics/odds_ratio.md)

## Sequencing

- [QC](sequencing/qc.md)
- [Analysis](sequencing/analysis.md)
- [Software](sequencing/software.md)
- [Technique](sequencing/technique.md)
- [Troubleshooting](sequencing/troubleshooting.md)

## Type setting

- [LaTeX](type_setting/latex.md)
- [Markdown](type_setting/markdown.md)
- [Rmarkdown](type_setting/rmarkdown.md)
- [Unicode](type_setting/unicode.md)

## Web

- [AWS Lightsail](web/aws_lightsail.md)
- [Google analytics](web/google_analytics.md)
