# Troubleshooting

## Too many differentially expressed genes in RNA-Seq

This may be because the compared conditions are really very different, for example, samples from different organs/tissues. Also a very drastic drug effect will make the entire transcriptome is changing *en masse*, e.g., due to apoptosis.

Standard tools for differential expression analysis tools (e.g. edgeR and DESeq2) assume that most genes in the samples are equally expressed, and only a small fraction of genes are differentially expressed.[^1] This is because (all) the normalization methods rely on the assumption that most of the genes are not differentially expressed.[^2] When this normalization assumptions are violated, the DE analysis results should not be trusted.[^3]

However, the packages are robust to having a sizeable proportion (**how large?**) of truly differentially expressed genes, as long there are also a lot of non-differentially expressed genes for parameter estimation.[^4]

**The question is how could we know the normalization assumption is violated or not?**

> Generally, I would not consider the number of DE genes to be indicative of whether TMM normalization is okay or not. If you have a very high-powered experiment, you will detect many DE genes at a given significance threshold, even if most of them have near-zero log-fold changes and do not cause any meaningful violation of TMM's assumption. Conversely, failure to detect many DE genes does not mean that TMM normalization is suitable, given that the DE analysis already assumes that normalization was correct.[^5]

2 possible solutions for too-many-DEG problem caused by comparing very different samples are:

- Add spike-in control in sequencing and then use it for normalization[^3] (if no spike-in controls, perhaps normalize to some housekeeping genes instead?).
- Use stricter threshold for DEG analysis, including lower adjusted p-values (`< 0.05`?) and higher log2 fold change (`< -1` and `> 1`?).[^5]

    - For edgeR, set `logratioTrim` in the function `calcNormFactors()`.[^3]
    - For DESeq2, Use the arguments `alpha`, `lfcThreshold` and `altHypothesis` from `results()` to set the desired LFC threshold and test.[^5]

    A volcano plot could be made to view the threshold.[^5] The code:

    ``` R
    DESeq.Result$super <- DESeq.Result$log2FoldChange >  1  & DESeq.Result$padj < 0.01
    DESeq.Result$sub   <- DESeq.Result$log2FoldChange < -1  & DESeq.Result$padj < 0.01
    DESeq.Result$threshold <- as.factor(abs(DESeq.Result$log2FoldChange) > 1 & DESeq.Result$padj < 0.01)

    volcano_plot <- ggplot(data=DESeq.Result, aes(x=log2FoldChange, y=-log10(padj))) +
      geom_point(data=DESeq.Result, size=1, colour="gray") +
      geom_point(data=DESeq.Result[DESeq.Result$super==TRUE, ], size=2, colour="#CC0000") +
      geom_point(data=DESeq.Result[DESeq.Result$sub  ==TRUE, ], size=2, colour="#000099") +
      xlab("log2 fold change") +
      ylab("-log10 p-value adjusted") +
      ggtitle("My Title")+
      scale_x_continuous() +
      scale_y_continuous() +
      theme_bw() +
      theme(axis.title.y = element_text(face="bold", size=16),
            axis.title.x = element_text(face="bold", size=16, colour="black"),
            axis.text = element_text(size=12),
            legend.title =element_blank() ,
            legend.text = element_text(size = 12)) +
      theme(plot.title = element_text(hjust = 0.5))
    volcano_plot
    ```

Another possible reason for too-many-DEG problem is the number of biological/technical replicates are not enough to estimate the in-group variance of gene expression or the replicates could not capture enough biological variability. Therefore, even small differences between conditions are judged as significant by edgeR/DESeq2.[^6] To solve this problem, one should either add more replicates or use stricter threshold for DE analysis.[^6]

## Filtering out genes with low reads count

### Is this necessary?

This is not necessary. DESeq2 automatically perform this (independent filtering) within `result()`.[^7]

> The results function of the DESeq2 package performs independent filtering by default using the mean of normalized counts as a filter statistic. A threshold on the filter statistic is found which optimizes the number of adjusted p values lower than a significance level alpha (we use the standard variable name for significance level, though it is unrelated to the dispersion parameter α). The theory behind independent filtering is discussed in greater detail in Section 4.6. The adjusted p values for the genes which do not pass the filter threshold are set to NA.[^7]

However, perform this manually would reduce the memory size used by the objects and reduce the computation time.[^8]

One can use `independentFiltering=FALSE` to turn off the automatically performed filtering in `result()`.[^9]

### Should this step be performed before or after normalization?

This step should be performed after normalization.[^10]

For DESeq2:[^9]

``` R
dds <- estimateSizeFactors(dds)
idx <- rowSums( counts(dds, normalized=TRUE) >= 5 ) >= 3

dds <- dds[idx,]
dds <- DESeq(dds)
```

## When using salmon, how to deal with unstranded transcripts?

Question how to deal with unstranded features?[^11]

> First of all, the libtype should be ISR. Then some (not all) of the transcripts that I interested in is unstranded (actually they are on only one strand, but we don't know which one). What should I do for this?
>
>    1. assuming they are on + strand, and get the sequences of transcripts. Then run salmon with libtype IU.
>    1. assuming they are on + strand, and get the sequences of transcripts. Then run salmon with libtype ISR and set `--incompatPrior` a non-zero value (but what value is suitable?).
>    1. get 2 sequences for each of those transcripts, one for + strand and one for - strand. Then run salmon with libtype ISR. Finally sum the counts of the 2 sequences.
>
> Which way is more rational, or is there any better way to do?

Answer from the author:

> both approaches (3) and (2) seem reasonable to me. I would not try approach (1) as this will eliminate the benefit of the stranded library for the targets where you do know the orientation. For approach (2) , I'd either use `--validateMappings` or at least set `--rangeFactorizationBins 4` (the former implies the latter). As for what value to set for `--incompatPrior`, the effect should be reasonably robust across a range of values, the question is how unlikely a priori would you expect a mapping not in ISR orientation to be if you also observed a mapping in ISR ... probably very unlikely (you could try e.g. 1e-10 or some such). Approach 3 is also also reasonable, though what you might consider doing is looking at the abundances for these opposite strands of the same sequence post quantification --- you should generally see that one of the two has a non-zero expression, or at least one orientation should have a much higher expression than the other (for expressed transcripts, at least, this might give you evidence as to the true strand of origin).

## Difference between genome annotation

The Ensembl and GENCODE annotation seems to be very similar, they all contain the automatic annotation from Ensembl's automatic pipeline and manually curated annotation from Havana team.

From Ensembl:[^12]

> In the current release, we continue to display a joint gene set based on the merge between the automatic annotation from Ensembl and the manually curated annotation from Havana.
>
> Updated manual annotation from Havana is merged into the Ensembl annotation every release. Transcripts from the two annotation sources are merged if they share the same internal exon-intron boundaries (i.e. have identical splicing pattern) with slight differences in the terminal exons allowed.

From GENCODE:[^13]

> The GENCODE annotation is made by merging the manual gene annotation produced by the Ensembl-Havana team and the Ensembl-genebuild automated gene annotation. The GENCODE annotation is the default gene annotation displayed in the Ensembl browser. 

The main difference between the GTF files from GENCODE and Ensembl is at the human chromosome X and Y PAR regions:[^13]

> The gene annotation is the same in both files. The only exception is that the genes which are common to the human chromosome X and Y PAR regions can be found twice in the GENCODE GTF, while they are shown only for chromosome X in the Ensembl file.

Therefore, it seem more rational to use annotation from Ensembl as reference.

## 'starts too early' error from dexseq_prepare_annotation.py

This error arises when 2 exons from different strands have the same 'gene_id' in the input GTF file (like dual version for unstranded vlinc).

## Which human reference genome to use?

1. It is much better to use build 38 than build 37.[^15]
1. For build 38, use hg38 from bcbio-nextgen.
    - The support of hg38-noalt is being dropped by bcbio-nextgen.[^16]
    - hg38 from bcbio-nextgen contains alt alleles in the *genome sequence*, but not in the *annotation GTF*.
    - For hg38, hisat2 but not STAR should be use as the aligner, since hisat2 handles the alts correctly while STAR does not.
1. For build 37, use GRCh37 over hg19 from bcbio-nextgen.
    - GRCh37 has masked PAR on chromosome Y.
    - GRCh37 and hg19 do not contain alt alleles.
    - Note: GRCh37 use chromosome name 1, 2, 3, while hg19 use chr1, chr2, chr3.

Potential issues of reference genome:[^14]

> There are several other versions of GRCh37/GRCh38. What’s wrong with them? Here are a collection of potential issues:
>
>   1. Inclusion of ALT contigs. ALT contigs are large variations with very long flanking sequences nearly identical to the primary human assembly. Most read mappers will give mapping quality zero to reads mapped in the flanking sequences. This will reduce the sensitivity of variant calling and many other analyses. You can resolve this issue with an ALT-aware mapper, but no mainstream variant callers or other tools can take the advantage of ALT-aware mapping.
>
>   2. Padding ALT contigs with long “N”s. This has the same problem with 1 and also increases the size of genome unnecessarily. It is worse.
>
>   3. Inclusion of multi-placed sequences. In both GRCh37 and GRCh38, the pseudo-autosomal regions (PARs) of chrX are also placed on to chrY. If you use a reference genome that contains both copies, you will not be able to call any variants in PARs with a standard pipeline. In GRCh38, some alpha satellites are placed multiple times, too. The right solution is to hard mask PARs on chrY and those extra copies of alpha repeats.
>
>   4. Not using the rCRS mitochondrial sequence. rCRS is widely used in population genetics. However, the official GRCh37 comes with a mitochondrial sequence 2bp longer than rCRS. If you want to analyze mitochondrial phylogeny, this 2bp insertion will cause troubles. GRCh38 uses rCRS.
>
>   5. Converting semi-ambiguous IUB codes to “N”. This is a very minor issue, though. Human chromosomal sequences contain few semi-ambiguous bases.
>
>   6. Using accession numbers instead of chromosome names. Do you know CM000663.2 corresponds to chr1 in GRCh38?
>
>   7. Not including unplaced and unlocalized contigs. This will force reads originated from these contigs to be mapped to the chromosomal assembly and lead to false variant calls.
>
> Now we can explain what is wrong with other versions of human reference genomes:
>
>   - hg19/chromFa.tar.gz from UCSC: 1, 3, 4 and 5.
>   - hg38/hg38.fa.gz from UCSC: 1, 3 and 5.
>   - GCA_000001405.15_GRCh38_genomic.fna.gz from NCBI: 1, 3, 5 and 6.
>   - Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz from EnsEMBL: 3.
>   - Homo_sapiens.GRCh38.dna.toplevel.fa.gz from EnsEMBL: 1, 2 and 3.

## Create tx2gene.csv from GTF 

Use 'GenomicFeatures' package.

``` R
library('GenomicFeatures')
library('tidyverse')

txdb <- makeTxDbFromGFF(file = "./ref-transcripts.gtf", format = 'gtf')

k <- keys(txdb, keytype = "TXNAME")
tx2gene <- select(txdb, k, "GENEID", "TXNAME")

write_csv(tx2gene, "./tx2gene.csv", col_names = F)
```

## `results()` vs `lfcShrink()` in DESeq2

In DESeq2, `results()` and `lfcShrink()` are two different frameworks, and perform different statistic tests.[^17]

- `results()` tests whether the *non-shrunken* log2FoldChange (lfc) is greater or lesser than the `lfcThreshold`. For example, `results(dds, lfcThreshold = 1, altHypothesis = 'greaterAbs')` tests whether the absolute value of the *non-shrunken* lfc is greater than 1 (default `alpha = 0.1`, i.e. `adjusted p-value < 0.1`).

- `lfcShrink()` tests whether the *shrunken* lfc is greater or lesser than the `lfcThreshold`. For example, `lfcShrink(dds, coef, lfcThreshold = 1, type = 'apeglm')` tests whether the absolute value of the *shrunken* lfc is greater than 1. It uses 's-value' other than 'p-value', with the default alpha 0.005.

**Note**: `lfcShrink(dds, coef, lfcThreshold = 0, svalue = FALSE)` (the default), it will not output 's-value', but replace lfc with *shrunken* lfc and keep the 'p-value'. However, those p-values are for the test of *non-shrunken* lfc.

Therefore, there are 3 possible workflows:

1. Use `res <- results(dds, alpha = 0.05, lfcThreshold = 1, altHypothesis = 'greaterAbs')` to get genes with *non-shrunken* lfc significantly (`adjusted p-value < 0.05`) `> 1` or `< -1`.

1. Use `res <- lfcShrink(dds, coef, lfcThreshold = 1, type = 'apeglm')` to get genes with *shrunken* lfc significantly (`s-value < 0.005`) `> 1` or `< -1`.

1. Use `res <- results(dds, alpha = 0.05)` to get genes with *non-shrunken* lfc significantly (`adjusted p-value < 0.05`) `> 0` or `< 0`. Then use `res <- lfcShrink(dds, coef, res = res, lfcThreshold = 0, svalue = FALSE)` to replace *non-shrunken* lfc with *shrunken* lfc. Finally manually filter genes with *shrunken* lfc `> 1` or `< -1`.

The 1st method uses *non-shrunken* lfc. The 2nd method uses 's-value' which is not popular and hard to explain in paper. The 3rd method is a mixture, it uses 'p-value' for *non-shrunken* lfc on a threshold and then filter genes with *shrunken* lfc on another threshold.

The most correct method is the 2nd one, but it's hard to explain. The 3rd method may be the most uncorrect one, but it uses 'p-value' and *shrunken* lfc, and could be easy to explain.

## Selecting uniquely mapped reads

### Bowtie2

`AS` tag indicates it is mapped and `XS` tag indicates it has multiple mapping results[^18][^19].

``` bash
samtools view R-R-SO_length30_WP_1.bam |grep 'AS'|grep -v 'XS'
```

### Selecting reads with high quality mapping result instead

It may make more sense to select reads by its mapping quality (MapQ)[^20].
The $$MapQ = -10*log10(P)$$, where $$P$$ is the probability that this mapping result is wrong.
Therefore, MapQ 10, 20, 30 means the mapping result has 0.1%, 0.01% and 0.001% probability to be wrong, respectively.

``` bash
samtools view -q 20 t.bam > t_Q20.bam
```

---

## References

[^1] https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-14-91

[^2] http://seqanswers.com/forums/showthread.php?t=35039

[^3] https://support.bioconductor.org/p/94661/#94680

[^4] https://www.biostars.org/p/262676/#262708

[^5] https://www.biostars.org/p/294859/

[^6] https://support.bioconductor.org/p/53957/

[^7] https://support.bioconductor.org/p/63234/

[^8] https://www.bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html#pre-filtering

[^9] https://support.bioconductor.org/p/65256/

[^10] http://seqanswers.com/forums/showthread.php?t=49460

[^11] https://github.com/COMBINE-lab/salmon/issues/332

[^12] https://asia.ensembl.org/Homo_sapiens/Info/Annotation

[^13] https://www.gencodegenes.org/pages/faq.html

[^14] https://lh3.github.io/2017/11/13/which-human-reference-genome-to-use

[^15] https://bcbio-nextgen.readthedocs.io/en/latest/contents/configuration.html#reference-genome-files

[^16] https://github.com/bcbio/bcbio-nextgen/issues/2568

[^17] https://support.bioconductor.org/p/110307/

[^18] http://www.biotrainee.com/thread-1115-1-1.html

[^19] https://biofinysics.blogspot.com/2014/05/how-does-bowtie2-assign-mapq-scores.html

[^20] https://qiubio.com/archives/3321
