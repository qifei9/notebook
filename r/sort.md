# Sort

## Mixedsort

### Use gtools

Use `mixedsort` to sort,[^1] and use `mixedorder` to get order.

``` R
> library("gtools")
> mixedsort(c("v10", "v7", "v2"))
[1] "v2" "v7"  "v10"
> mixedorder(c("v10", "v7", "v2"))
[1] 3 2 1
```

### Use tidyverse

To sort the columns of a dataframe, use `num_range()` together with `select()` from tidyverse.[^2]

``` R
> library("tidyverse")
> select(dataframe, num_range("v", 2:10))
```

---

## References

[^1] https://stackoverflow.com/questions/17531403

[^2] https://dplyr.tidyverse.org/reference/select.html
