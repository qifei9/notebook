# Google analytics

## 账户构架

账户构架：

> Google 账户(1) --- (N) GA 账户 (1) --- (N) 媒体资源 (1) --- (N) 数据视图 

一个谷歌账户可以拥有多个 Google Analytics 账户 (accounts)。 

Google Analytics 账户建议实践：

> **网站服务**：建议以**域名**区分。通常同一个域名下的每一个子域名都会有相关性，归属在同一个账户较方便管理。
>
> **其他服务**：只要可以联网的都可以使用 Google Analytics，所以可以依照服务属性或品牌区分。例如硬件厂商的品牌 A 和品牌 B。

媒体资源 (properties) 代表的是一个数据追踪的数据集合， 这个数据集合拥有一个唯一识别的 ID，称为跟踪ID (Tracking ID)。在创建这个媒体资源时会同时创建一个未过滤的数据视图用来接收你的所有追踪数据。

媒体资源建议实践：

> **网站服务**：建议以不同的**子域名**区分，原因是通常一个媒体资源会牵涉到的是和其他产品的链接，多个子域名结合在一个媒体资源内会造成后续分析的难度提高，例如自然流量的分析，转换计数错误等等。再来很重要的就是会影响收集数据的准确度。

## Adding to a website

First, Create a GA account with the URL of your website. Then get the Tracking ID.

### Hugo + Academic

Add `googleAnalytics = "your_tracking_ID"` to `config.toml`.

## References

[^1] https://zhuanlan.zhihu.com/p/50903165
