# Pandas

## Selection

``` python
record.annotations['source'] = table.loc[
    table['Scientific name'] == species.capitalize(
    ).replace('_', ' '), 'Common name'].iloc[0].replace(' ', '_')
```

## Add rows to df

Use `df.append()`:[^1]

``` python
# A continuous index value will be maintained 
# across the rows in the new appended data frame if ignore_index = True
df.append(df2, ignore_index = True) 
```

## map, applymap and apply

Summing up, `apply` works on a row / column basis of a DataFrame, `applymap` works element-wise on a DataFrame, and `map` works element-wise on a Series.[^2]

> Another frequent operation is applying a function on 1D arrays to each column or row. DataFrame’s apply method does exactly this:

``` python
In [116]: frame = DataFrame(np.random.randn(4, 3), columns=list('bde'), index=['Utah', 'Ohio', 'Texas', 'Oregon'])

In [117]: frame
Out[117]: 
               b         d         e
Utah   -0.029638  1.081563  1.280300
Ohio    0.647747  0.831136 -1.549481
Texas   0.513416 -0.884417  0.195343
Oregon -0.485454 -0.477388 -0.309548

In [118]: f = lambda x: x.max() - x.min()

In [119]: frame.apply(f)
Out[119]: 
b    1.133201
d    1.965980
e    2.829781
dtype: float64
```

> Many of the most common array statistics (like sum and mean) are DataFrame methods, so using apply is not necessary.
>
> Element-wise Python functions can be used, too. Suppose you wanted to compute a formatted string from each floating point value in frame. You can do this with applymap:

``` python
In [120]: format = lambda x: '%.2f' % x

In [121]: frame.applymap(format)
Out[121]: 
            b      d      e
Utah    -0.03   1.08   1.28
Ohio     0.65   0.83  -1.55
Texas    0.51  -0.88   0.20
Oregon  -0.49  -0.48  -0.31
```

> The reason for the name applymap is that Series has a map method for applying an element-wise function:

``` python
In [122]: frame['e'].map(format)
Out[122]: 
Utah       1.28
Ohio      -1.55
Texas      0.20
Oregon    -0.31
Name: e, dtype: object
```

## Iterate over rows in Pandas dataframe

Create a df:[^3]

``` python
raw_data = {'name': ['Willard Morris', 'Al Jennings', 'Omar Mullins', 'Spencer McDaniel'],
		        'age': [20, 19, 22, 21],
		        'favorite_color': ['blue', 'red', 'yellow', "green"],
		        'grade': [88, 92, 95, 70]}
df = pd.DataFrame(raw_data, columns = ['name', 'age', 'favorite_color', 'grade'])
```

Using `iterrows`:

``` python
for index, row in df.iterrows():
    print (row["name"], row["age"])
```

Using `itertuples`:

``` python
for row in df.itertuples(index=True, name='Pandas'):
    print (getattr(row, "name"), getattr(row, "age"))
```

If you wish to modify the rows you're iterating over, then df.apply is preferred:

``` python
def valuation_formula(x):
    return x * 0.5

df['age_half'] = df.apply(lambda row: valuation_formula(row['age']), axis=1)

df.head()
```

## Create a column according to another column

Use `map`:[^4]

``` python
df['涨跌'] = df['p_change'].map(lambda x: (x<0 and '跌') or (x==0 and '平') or (x>0 and '涨'))
```

---

## References

[^1] https://www.geeksforgeeks.org/python-pandas-dataframe-append/

[^2] https://stackoverflow.com/a/19798528/10493956

[^3] https://erikrood.com/Python_References/iterate_rows_pandas.html

[^4] https://www.zhihu.com/question/40919460
