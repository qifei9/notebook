# REPL

REPL: Read-Eval-Print Loop

## R

Use [Nvim-R](https://github.com/jalvesaq/Nvim-R).

## Python

Use [vimcmdline](https://github.com/jalvesaq/vimcmdline).

``` vim
" vimcmdline
" make the output is highlighted using your current colorscheme
"let cmdline_follow_colorscheme = 1
"
" vimcmdline mappings
let cmdline_map_start          = '<LocalLeader>rf'
let cmdline_map_send           = '<LocalLeader>d'
let cmdline_map_send_and_stay  = '<LocalLeader>l'
let cmdline_map_source_fun     = '<LocalLeader>f'
let cmdline_map_send_paragraph = '<LocalLeader>pe'
let cmdline_map_send_block     = '<LocalLeader>b'
let cmdline_map_quit           = '<LocalLeader>rq'

" vimcmdline options
let cmdline_vsplit      = 1      " Split the window vertically
let cmdline_esc_term    = 1      " Remap <Esc> to :stopinsert in Neovim's terminal
let cmdline_in_buffer   = 1      " Start the interpreter in a Neovim's terminal
"let cmdline_term_height = 15     " Initial height of interpreter window or pane
let cmdline_term_width  = 90     " Initial width of interpreter window or pane
let cmdline_tmp_dir     = '/tmp' " Temporary directory to save files
let cmdline_outhl       = 1      " Syntax highlight the output
let cmdline_auto_scroll = 1      " Keep the cursor at the end of terminal (nvim)

"change the REPL for filetype
let cmdline_app           = {}
let cmdline_app['python'] = 'ipython --no-autoindent'

" set \rp to print a object
autocmd FileType python nmap <buffer> <LocalLeader>rp :call VimCmdLineSendCmd(expand('<cword>'))<CR>
```

Note:
The support for ipython is not perfect.[^1]

## Python and R

### Use `reticulate` and Nvim-R

> The [**reticulate**](https://rstudio.github.io/reticulate/index.html) package provides a comprehensive set of tools for interoperability between Python and R. 

1. Install `reticulate` package in R.
1. Install Nvim-R plugin for Neovim.
1. Edit an Rmarkdown file with Neovim.
1. Write python code in python chunk and R code in R chunk.
1. Open an R console with `<LocalLeader>rf`.
1. Then the python code and R code could be sent to the R console by Nvim-R.

---

## References

[^1] https://github.com/jalvesaq/vimcmdline/issues/26
