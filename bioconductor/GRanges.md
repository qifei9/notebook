# GRanges

## Merge GRanges objects

[^1]

``` R
gr_merged <- c(gr1, gr2)
```

## Sort GRanges objects

[^2]

``` R
gr <- sortSeqlevels(gr)
gr <- sort(gr, ignore.strand = TRUE)
```

Set `ignore.strand = TRUE` will ignore strand in sort, otherwise all '+' objects in a chromsome will be placed before '-' objects.

---

## References

[^1] https://support.bioconductor.org/p/84087/#84088

[^2] https://support.bioconductor.org/p/62960/
