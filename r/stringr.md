# stringr

## replace multiple patterns

``` r
fruits <- c("one apple", "two pears", "three bananas")
str_replace_all(fruits, c("one" = "1", "two" = "2", "three" = "3"))
```

## get elements matching a pattern from vector

``` r
str_subset(vector, pattern)
```
