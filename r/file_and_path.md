# File and Path

## Concatenate paths

Use `file.path()`.[^1]

``` R
> file.path("usr", "local", "lib")
[1] "usr/local/lib"

> file.path("usr", c("share", "bin"))
# [1] "usr/share" "usr/bin"  
```

## List files and dirs

### Files

``` R
list.files("./dir", recursive = T, full.names = T)
```

- `recursive` -- whether to include files in sub dir.
- `full.names` -- whether to include the path or only include filenames.
- `pattern` -- only return files whose name match the pattern.
- `include.dirs` -- whether to include dirs or only files.
- `no..` -- whether or not to include `./` and `../`.
- `all.files` -- whether or not to include hidden files.

### Dirs

``` R
list.dirs("./dir", recursive = T, full.names = T)
```

## Get file basename

``` R
> basename(blood_path$path[1])
[1] "BFZ-1_HG37VCCXY_L7_1.fq.gz"
```

## Create dir

Use `dir.create()`:[^2]

``` R
dir.create(file.path("testdir2","testdir3"), recursive = TRUE)
```

## Copy file

Use `file.copy`:[^3]

``` R
flist <- list.files("patha", "^filea.+[.]csv$", full.names = TRUE)
file.copy(flist, "pathb")
```

## Get absolute path of files

Use `normalizePath()`:[^4]

``` r
normalizePath(list.files(file))
```

---

## References

[^1] https://stackoverflow.com/questions/13110076/function-to-concatenate-paths

[^2] https://stackoverflow.com/questions/49110819/r-command-dir-create-and-file-path

[^3] https://stackoverflow.com/a/2384621/10493956

[^4] https://stackoverflow.com/questions/13311180/how-do-i-get-the-absolute-path-of-an-input-file-in-r
