# Read .mat file from MATLAB

## .mat before v7.3

``` python
import scipy.io
mat = scipy.io.loadmat('file.mat')
mat['<colname>']
mat['<colname>'].shape
```

The `mat` is an array of arrays of arrays. For example, if in `file.mat` there are 3 columns and 4 rows, then `mat` contains 3 arrays, each for one columns, and every array in `mat` contains 4 arrays each for a row in that column.

## .mat of v7.3

>Neither scipy.io.savemat, nor scipy.io.loadmat work for matlab arrays --v7.3. But the good part is that matlab --v7.3 files are hdf5 datasets. So they can be read using a number of tools, including numpy.
>
>For python, you will need the h5py extension, which requires HDF5 on your system.
>
>``` python
>import numpy as np
>import h5py 
>f = h5py.File('somefile.mat','r') 
>data = f.get('data/variable1') 
>data = np.array(data) # For converting to numpy array
>```

## References

[^1] https://stackoverflow.com/questions/874461/read-mat-files-in-python
