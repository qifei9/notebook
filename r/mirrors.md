# Mirrors

Use [TUNA](https://mirrors.tuna.tsinghua.edu.cn/) from Tsinghua.

## CRAN

In `~/.Rprofile`, add:

``` r
options("repos" = c(CRAN="https://mirrors.tuna.tsinghua.edu.cn/CRAN/"))
```

## Bioconductor

In `~/.Rprofile`, add:

``` r
options(BioC_mirror="https://mirrors.tuna.tsinghua.edu.cn/bioconductor")
```
