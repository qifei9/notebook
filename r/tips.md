# Tips

## write

- align colnames to columns

    ``` r
    a <- data.frame(a = c(1:3), b = c(1:3))

    write.table(a, "./a1.txt")
    # a1.txt:
    #"a" "b"
    #"1" 1 1
    #"2" 2 2
    #"3" 3 3

    write.table(a, "./a2.txt", col.names = NA)
    # a2.txt:
    #"" "a" "b"
    #"1" 1 1
    #"2" 2 2
    #"3" 3 3
    ```

- write vector to a file using `write()`

    ``` r
    write(glengths, file="data/genome_lengths.txt", ncolumns=1)
    ```

## match

``` r
first <- c("A","B","C","D","E")
second <- c("B","D","E","A","C")  # same letters but different order

match(first,second)
#[1] 4 1 5 2 3
```
