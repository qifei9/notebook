# Tips

- remove unwanted whitespace[^1]

    ``` vim
    " trailing spaces
    :%s/\s\+$//e

    " beginning spaces
    :%s/^\s\+//e
    " Same thing (:le = :left = left-align given range; % = all lines):
    :%le
    ```

---

## References

[^1] https://vim.fandom.com/wiki/Remove_unwanted_spaces
