# wallpaper

## Use Windows spotlight for wallpaper

Use [spotlight](https://github.com/mariusknaust/spotlight).

``` bash
# install
yay -S spotlight

# enable
systemctl --user enable spotlight.timer
```
