# Fold

## Set fold method in file

Add the following line to a file will make it folded by `marker` method in vim/neovim.

```
# vim:fdm=marker
```
