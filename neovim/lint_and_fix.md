# Lint and fix

Use [ALE](https://github.com/dense-analysis/ale).

## General setting

``` vim
let g:ale_sign_error   = '•'
let g:ale_sign_warning = '•'
let g:ale_linters = {
    \'python': ['pyls'],
\}
let g:ale_fixers = {'python': ['yapf', 'isort'], 'r': ['styler']}
let g:ale_r_lintr_options = 'lintr::with_defaults(commented_code_linter = NULL, single_quotes_linter = NULL, object_name_linter = NULL)'
let g:ale_r_styler_options = 'styler::tidyverse_style, indent_by = 4'
let g:ale_sign_column_always = 1
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
nmap <F8> <Plug>(ale_fix)
" Remap keys for gotos
nmap <silent> gd <Plug>(ale_go_to_definition)
nmap <silent> gy <Plug>(ale_go_to_type_definition)
nmap <silent> gr <Plug>(ale_find_references)
" Use K to show documentation
nmap <silent> K <Plug>(ale_hover)
" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(ale_previous)
nmap <silent> ]g <Plug>(ale_next)
```


## R

Use [`lintr`](https://github.com/jimhester/lintr) for lint and [`styler`](https://github.com/r-lib/styler) as fixer.

``` vim
" disable some lint in lintr
let g:ale_r_lintr_options = 'lintr::with_defaults(commented_code_linter = NULL, single_quotes_linter = NULL, object_name_linter = NULL)'
" set fixer
let g:ale_fixers = {'r': ['styler']}
" set indentation to 4 spaces for styler
let g:ale_r_styler_options = 'styler::tidyverse_style, indent_by = 4'
```

**NOTE:** The `let g:ale_r_styler_options = 'styler::tidyverse_style, indent_by = 4'` is a trick. The value of `ale_r_styler_options` goes to the `style` parameter of `styler::style_file()`, and `style_file(file, style = tidyverse_style(indent_by = 4))` doesn't work. I guess when setting `ale_r_styler_options = 'styler::tidyverse_style, indent_by = 4'`, the command becomes `style_file(file, style = tidyverse_style, indent_by = 4)` which is known working.

## Python

Use `pyls` for lint, and `yapf` and `isort` as fixer.

``` vim
let g:ale_linters = {'python': ['pyls']}
let g:ale_fixers  = {'python': ['yapf', 'isort']}
```
