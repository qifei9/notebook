# checkout

## Checkout a file from another branch

``` bash
git checkout [branch] -- [file name]
```

The file would be checked out and automatically staged.
