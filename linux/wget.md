# wget

## Proxy

[^1]

``` bash
export SOCKS_SERVER=127.0.0.1:1080
wget http://server-C/whatever
```

---

## References

[^1] https://unix.stackexchange.com/questions/38755/how-to-download-a-file-through-an-ssh-server
