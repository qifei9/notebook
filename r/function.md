# Function

## Arguments

### Test whether the argument is specified

Use `missing()`[^1]:

``` r
fooBar <- function(x,y){
    if(missing(y)) {
        x
    } else {
        x + y
    }
}

fooBar(3,1.5)
# [1] 4.5
fooBar(3)
# [1] 3
```

---

## References

[^1] https://stackoverflow.com/a/28370319/10493956
