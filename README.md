# qifei9's notebook

[![pipeline status](https://gitlab.com/qifei9/notebook/badges/master/pipeline.svg)](https://gitlab.com/qifei9/notebook/commits/master)

This is my notebook powered by GitBook and hosted no GitLab Pages: https://qifei9.gitlab.io/notebook/.
