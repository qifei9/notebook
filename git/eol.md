# EOL

## Auto convert the EOL

### Linux

1. `git config --global core.autocrlf input`
1. `git config --global core.eol native`
1. add `.gitattributes` like:

    ``` gitattributes
    # These files are text and should be normalized (convert crlf to lf)
    *.cs      text diff=csharp
    *.xaml    text
    *.csproj  text
    *.sln     text
    *.tt      text
    *.ps1     text
    *.cmd     text
    *.msbuild text
    *.md      text

    # Images should be treated as binary
    # (binary is a macro for -text -diff)
    *.png     binary
    *.jepg    binary

    *.sdf     binary

    *       text=auto
    ```

1. commit `.gitattributes`

## Refresh after changeling EOL setting

1. Save your current files in Git, so that none of your work is lost.

    ``` bash
    git add . -u
    git commit -m "Saving files before refreshing line endings"
    ```

1. Add all your changed files back and normalize the line endings.

    ``` bash
    git add --renormalize .
    ```

1. Show the rewritten, normalized files.

    ``` bash
    git status
    ```

1. Commit the changes to your repository.

    ``` bash
    git commit -m "Normalize all the line endings"
    ```

1. `git rm --cached -r .`
1. `git reset --hard`

---

## References

[^1] https://adaptivepatchwork.com/2012/03/01/mind-the-end-of-your-line/
[^2] https://help.github.com/en/github/using-git/configuring-git-to-handle-line-endings
