# Conditional operator

``` python
condition_is_true if condition else condition_is_false
```

``` python
is_fat = True
state = "fat" if is_fat else "not fat"
```

---

## References

[^1] https://eastlakeside.gitbooks.io/interpy-zh/content/ternary_operators/ternary_operators.html
