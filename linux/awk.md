# awk

## Filter rows

``` bash
awk '{if($5==2) print}' input
```

## Edit one column

``` bash
awk '{$4 = $4"#"1}1' input
```

## Edit file in place

``` bash
awk -i inplace '{$4=$4"#"2}1' OFS="\t" input
```

## Separation of output

Set `OFS="\t"`.

## Split file by column(s)

``` bash
awk 'NR==1 { H=$0; next } { file=$1".bg" } { if(!d[file]) print H >> file; print $0 >> file; d[file]=1 }' file.bg
```
