# Path

## List the files in a dir

``` python
import glob
files = [f for f in glob.glob("./*.fa")]
```

Note: the return of `glob.glob` is unsorted (in the same order as `ls -U`). To sort it:[^2]

``` python
# sort by name:

sorted(glob.glob('*.png'))

# sort by modification time:

import os
sorted(glob.glob('*.png'), key=os.path.getmtime)

# sort by size:

import os
sorted(glob.glob('*.png'), key=os.path.getsize)
```

## Get the basename of a file

``` python
from pathlib import Path

Path('/root/dir/sub/file.ext').stem

# will print 'file'


# If the path can be a symbolic link, then add resolve()
Path('/root/dir/sub/file.ext').resolve().stem
```

---

## References

[^1] https://stackoverflow.com/a/47496703

[^2] https://stackoverflow.com/questions/6773584/how-is-pythons-glob-glob-ordered
