# shell

## commands

### grep

Use the `-B` and `-A` arguments for `grep` to return the matched line plus one before (`-B1`) and two lines after (`-A2`).

### basename

``` bash
basename ~/unix_lesson/raw_fastq/Mov10_oe_1.subset.fq
# return 'Mov10_oe_1.subset.fq'

basename ~/unix_lesson/raw_fastq/Mov10_oe_1.subset.fq .fq
# return 'Mov10_oe_1.subset'
```

## coding

``` bash
# assign variables
num=25
allfiles=`ls *.fq`

# loops
for x in *.fq
    do
        echo $x
        wc -l $x
    done
```

Example of a script[^1].

``` bash
#!/bin/bash 

# enter directory with raw FASTQs
cd ~/unix_lesson/raw_fastq

# count bad reads for each FASTQ file in our directory
for filename in *.fq 
do 

  # create a prefix for all output files
  base=`basename $filename .subset.fq`

  # tell us what file we're working on	
  echo $filename

  # grab all the bad read records
  grep -B1 -A2 NNNNNNNNNN $filename > ${base}-badreads.fastq

  # grab the number of bad reads and write it to a summary file
  grep -cH NNNNNNNNNN $filename >> badreads.count.summary
done
```

---

## References

[^1] https://hbctraining.github.io/Intro-to-Shell/lessons/04_loops_and_scripts.html
