# Unicode

## Input Unicode character

[^1]

1. `i` go into INSERT mode
1. `Ctrl+v` go into ins-special-keys mode
1. `u2713` insert the Unicode character `CHECK MARK (U+2713)`

---

## References

[^1] https://x-team.com/blog/inserting-unicode-characters-in-vim/
