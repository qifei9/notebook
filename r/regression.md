# Regression

## Nonlinear

Use `nls`:

``` r
pf_nr15a
# # A tibble: 4 x 2
#       t    pf
#   <dbl> <dbl>
# 1    15 0.423
# 2    30 0.665
# 3    60 0.838
# 4   120 1    

fit <- nls(
    pf~a+b*(1-exp(-k*t)),
    data = pf_nr15a,
    start = list(a = 0, b = 1.0, k = 0.1),
    trace = T
)

fit
# Nonlinear regression model
#   model: pf ~ a + b * (1 - exp(-k * t))
#    data: pf_nr15a
#       a       b       k 
# 0.11211 0.90703 0.02908 
#  residual sum-of-squares: 0.001304
# 
# Number of iterations to convergence: 7 
# Achieved convergence tolerance: 5.128e-06
```
