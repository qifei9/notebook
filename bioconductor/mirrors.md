# Mirrors

## Set mirror

First run:

``` R
chooseBioCmirror(local.only = T)
```

to select a mirror, then:

``` R
BiocManager::install(<package>)
```
