# Software

## Pipeline framework

### bcbio-nextgen

#### Installation

##### Automated

``` bash
wget https://raw.github.com/bcbio/bcbio-nextgen/master/scripts/bcbio_nextgen_install.py
python bcbio_nextgen_install.py /usr/local/share/bcbio --tooldir=/usr/local \
  --genomes GRCh37 --aligners bwa --aligners bowtie2
```

Note: The automated script use conda to install bcbio-nextgen. To accelerate, set mirrors in `.condarc` and edit `tmpbcbio-install/cloudbiolinux/contrib/flavor/ngs_pipeline_minimal/packages-conda.yaml` to avoid the mirrors being overwrote.[^3]

Currently (2019-01-08), Installation of 'deepvariant' by conda run into errors. Edit `tmpbcbio-install/cloudbiolinux/contrib/flavor/ngs_pipeline_minimal/packages-conda.yaml` to remove it from Installation.

##### bcbio-nextgen-vm

See the doc at github.[^7]

Note: The custom genome cannot be setup inside the container.[^6]

##### Use docker

1. Pull docker image(s).[^4]

    ``` bash
    docker pull quay.mirrors.ustc.edu.cn/bcbio/bcbio-rnaseq:latest
    docker pull quay.mirrors.ustc.edu.cn/bcbio/bcbio-vc:latest
    ```

2. Add Data.

    The data are linked to `/mnt/biodata`. Run a container in 'bind' mode with your local data directory mapped to `/mnt/biodata`.

    ``` bash
    docker run -it --mount type=bind,source=/mnt/Syn,target=/mnt quay.mirrors.ustc.edu.cn/bcbio/bcbio-vc:latest
    ```

    Then use `bcbio_nextge.py upgrade` inside the container to add data.

    ``` bash
    bcbio_nextgen.py upgrade --data --datatarget rnaseq --genomes hg38-noalt --aligners star --cores 36
    ```

    Note: 

    - The custom genome cannot be setup inside the container.[^6]
    - When installing the data, index of rtg is built by default. However, the *bcbio-rnaseq* image does not contain rtg-tools for building the index. Therefore, this index has to be firstly built by the *bcbio-vc* image (install data with *bcbio-vc* image and only build rtg index). Then the indices for the other aligners could be build by *bcbio-rnaseq* image.

#### Config

##### Set parameters for tools

Edit the template YAML to add 'resources' section:

``` yaml
details:
  - description: Example
resources:
  salmon:
    options: ["--validateMappings"]
```

Note: this could not alter the default parameters set by bcbio-nextgen, it appends the parameters to the cmd.

##### libType for salmon

The default `libType` for salmon is set to `IU` by bcbio-nextgen (since the default `platform` parameter is `illumina`?). It could be altered by setting the `strandedness` algorithm parameter. Set to `firststrand` for `--libType ISR`.

#### Pipeline

##### RNA-seq

- quality control
- adapter trimming
    - only perform adapter trimming when using Tophat2.
- alignment
    - use STAR aligner for all genomes without alt alleles.
    - use hisat2 for genomes with alt alleles.
    - only use Tophat2 when RAM < 30G.
- variant calling
- transcriptome reconstruction
- quantitation at gene and isoform levels
    - salmon quantitation is recommended rather than the counts from featureCounts to perform downstream quantification.[^5]

#### Running

Note: if bcbio-nextgen stopped by errors, re-run the pipeline will continue from where it stopped other than restart from the very beginning.

## Reads counting

### htseq-count

- [Repo](https://github.com/simon-anders/htseq)
- [Doc](https://htseq.readthedocs.io/en/latest/)
- **Really slow!!** About **20** fold slower than featureCounts.[^1]
- Cited more than featureCounts.[^1]

### featureCounts

- Part of [Subread package](http://subread.sourceforge.net/).
- Also available as an R package [Rsubread](http://bioconductor.org/packages/release/bioc/html/Rsubread.html).
- **Much faster** than htseq-count.[^1]
- Can take **multiple** BAM/SAM as input in a single call.
- Highly flexible in counting multi-mapping and multi-overlapping reads. Such reads can be excluded, fully counted or fractionally counted (different from htseq-count).[^2]

---

## References

[^1] http://bioinformatics.cvr.ac.uk/blog/featurecounts-or-htseq-count/

[^2] http://www.bioinfo-scrounger.com/archives/407

[^3] https://github.com/bcbio/bcbio-nextgen/issues/2607

[^4] https://github.com/bcbio/bcbio_docker

[^5] https://bcbio-nextgen.readthedocs.io/en/latest/contents/pipelines.html#rna-seq

[^6] https://github.com/bcbio/bcbio-nextgen-vm/issues/149

[^7] https://github.com/bcbio/bcbio-nextgen-vm
