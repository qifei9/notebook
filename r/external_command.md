# External command

## Run

`system2()` is recommended over `system()`.

``` R
system2(command, args)
```
