# List

## Get list element by name

``` r
get(name, list)
```

## Combine dataframes in a list

Use `ldply()` from `plyr` package[^1].

``` r
foldchange.drug.all <- plyr::ldply(foldchange.drug, data.frame, .id = NULL) %>%
    as_tibble()
```

`.id = NULL` suspends the creation of the 'id' column.

## References

[^1] https://stackoverflow.com/a/4227507/10493956
