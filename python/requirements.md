# requirements.txt

## Generate requirements.txt

``` bash
pip freeze > requirements.txt
```

Will add all installed modules into the requirements.txt.

``` bash
# first install pipreqs with pip or from distribution repo.
pipreqs ./
```

Will add only the modules imported in the scripts in `./`

## Install modules according to requirements.txt

``` bash
pip install -r requirements.txt
```
