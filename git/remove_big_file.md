# Remove big file from git repo

> **Warning:**
This process is not suitable for removing sensitive data like password or keys
from your repository. Information about commits, including file content, is
cached in the database, and will remain visible even after they have been
removed from the repository.

## File big files

``` bash
git rev-list --objects --all \
| git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' \
| sed -n 's/^blob //p' \
| sort --numeric-sort --key=2 \
| cut -c 1-12,41- \
| $(command -v gnumfmt || echo numfmt) --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest
```

## Using BFG

``` bash
cd my_repository/

git checkout master

git rm path/to/big_file.mpg
git commit -m 'Remove unneeded large file'

# delete a specific file
bfg --delete-files path/to/big_file.mpg
# or delete multiple files
bfg --delete-files *.mp4
# or delete files bigger than a size
bfg --strip-blobs-than 100M

git push --force-with-lease origin master
```

## Using `git filter-branch`

``` bash
cd my_repository/

git checkout master

git filter-branch --force --tree-filter 'rm -f path/to/big_file.mpg' HEAD

git reflog expire --expire=now --all && git gc --prune=now --aggressive

git push --force origin master
```

---

## References

[^1] https://stackoverflow.com/a/42544963/10493956

[^2] https://gitlab.com/help/user/project/repository/reducing_the_repo_size_using_git.md
