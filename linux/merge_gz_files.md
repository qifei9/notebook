# Merge gz files

``` bash
cat A.gz B.gz > merged.gz
```

---

## References

[^1] https://stackoverflow.com/questions/8005114/fast-concatenation-of-multiple-gzip-files
