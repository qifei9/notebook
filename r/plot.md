# Plot

## Venny diagram

### Use eulerr

``` r
library('eulerr')

# Input in the form of a named numeric vector
fit1 <- euler(c("A" = 25, "B" = 5, "C" = 5,
                "A&B" = 5, "A&C" = 5, "B&C" = 3,
                "A&B&C" = 3))

plot(fit1)

# Input as a matrix of logicals
set.seed(1)
mat <- cbind(
  A = sample(c(TRUE, TRUE, FALSE), 50, TRUE),
  B = sample(c(TRUE, FALSE), 50, TRUE),
  C = sample(c(TRUE, FALSE, FALSE, FALSE), 50, TRUE)
)
fit2 <- euler(mat)

plot(fit2)
```

**Note:**

- by default, circles are plotted, set `shape = 'ellipse'` in `euler()` to use ellipse.
- In `plot()`, use `quantities = quantities` to set numbers for quantities. The default is the numbers for each set. `quantities` could be a vector, the order of the numbers in the vector is the same as when 'input by named numeric vector', i.e. `A, B, C, A&B, A&C, B&C, A&B&C`.

**Snippet:**

``` r
deg_union <- unique(c(rownames(deg.PCvsH), rownames(deg.CvsH), rownames(deg.CvsPC)))

venny_table <- cbind(
    PCvsH = deg_union %in% rownames(deg.PCvsH),
    CvsH = deg_union %in% rownames(deg.CvsH),
    CvsPC = deg_union %in% rownames(deg.CvsPC)
)

venny_fit_c <- euler(venny_table)
venny_fit_e <- euler(venny_table, shape = 'ellipse')

get_quantity <- function (degunion, table, log1, log2, log3) { #{{{
    label <- venny_table[, 1] == log1 & venny_table[, 2] == log2 & venny_table[, 3] == log3
    n1 <- sum(str_detect(degunion[label], '^chr'))
    n2 <- sum(!str_detect(degunion[label], '^chr'))
    quantity = paste(n1, n2, sep = '/')
    return(quantity)
} #}}}

quantities <- c(
    get_quantity(deg_union, venny_table, 1, 0, 0),
    get_quantity(deg_union, venny_table, 0, 1, 0),
    get_quantity(deg_union, venny_table, 0, 0, 1),
    get_quantity(deg_union, venny_table, 1, 1, 0),
    get_quantity(deg_union, venny_table, 1, 0, 1),
    get_quantity(deg_union, venny_table, 0, 1, 1),
    get_quantity(deg_union, venny_table, 1, 1, 1)
)

pdf("../../result/DE_analysis/venny_PCCH_circle.pdf", width = 20.6, height = 11.8)
plot(venny_fit_c, quantities = quantities)
dev.off()

pdf("../../result/DE_analysis/venny_PCCH_ellipse.pdf", width = 20.6, height = 11.8)
plot(venny_fit_e, quantities = quantities)
dev.off()
```

## Heatmap

### Use pheatmap

In `pheatmap()`:

- set `filename = filename` to output plot to file.
- set `border_color = NA` to disable plotting the border of cells.
- to disable the name for annotation and legend, set `annotation_names_col = F` and for the `df` in `annotation_col = df`, set it names to a blank by `names(df) <- ' '` (note, here you have to use a blank, `NA`, `NULL` or `''` does not work, and if you have multiple series of legend, i.e. multipe columns in `df`, use `names(df) <- c(' ', ' ')`, untested).

``` r
heatmap_deg <- function (vsd, deg, description, levels, annos, name,
                         relative = FALSE, method = 'complete',
                         show_rownames = FALSE, cluster_cols = TRUE, order_des) {
    idx <- colData(vsd)[, description] %in% levels
    n <- assay(vsd)[rownames(deg), idx]
    if (relative) {
        n <- (n - rowMeans(n))/rowSds(n)
    }
    df <- as.data.frame(colData(vsd)[idx, annos])
    rownames(df) <- colnames(vsd)[idx]
    if (length(annos) < 2) {
        names(df) <- ' '
    }
    if (!cluster_cols) {
        n <- n[, rownames(df[order(df[, order_des]), ])]
    }
    filename <- file.path("../../result/DE_analysis", paste0(name, '.pdf'))
    pheatmap(n, cluster_rows = T, cluster_cols = cluster_cols,
             annotation_names_col = F,
             show_rownames = show_rownames,
             show_colnames = F,
             clustering_method = method,
             annotation_col = df,
             filename = filename,
             border_color = NA
    )
}
```

### Use ggplot2

``` r
p <- (
    ggplot(spinrates, aes(x = velocity, y = spinrate))
    + geom_tile(aes(fill = swing_miss))
    + scale_fill_distiller(palette = "YlGnBu")
)
```

## Empirical cumulative distribution

### Use ggplot2

``` r
df <- data.frame(
  x = c(rnorm(100, 0, 3), rnorm(100, 0, 10)),
  g = gl(2, 100)
)

ggplot(df, aes(x)) + stat_ecdf(geom = "step")

# Don't go to positive/negative infinity
ggplot(df, aes(x)) + stat_ecdf(geom = "step", pad = FALSE)

# Multiple ECDFs
ggplot(df, aes(x, colour = g)) + stat_ecdf()
```

## Barplot

### x-axis for barplot

`barplot()` returns the coordinates of the bars.[^1]

``` r
mp <- barplot(rainbar$Rain,axes=F,ylim=c(0,15))
axis(1,at=mp,labels=rainbar$DOY)
```

### barplot of percentage

[^6]

``` r
myplot <- (
    ggplot(tips, aes(day))
    + geom_bar(aes(y = (..count..)/sum(..count..)))
    + scale_y_continuous(labels=scales::percent)
)
```

or

``` r
p <- (
    ggplot(tips, aes(x= day,  group=sex))
    + geom_bar(aes(y = ..prop.., fill = factor(..x..)), stat="count")
    + geom_text(
        aes(label = scales::percent(..prop..), y= ..prop.. ),
        stat= "count", vjust = -.5
    )
    + labs(y = "Percent", fill="day") +
    + facet_grid(~sex) +
    + scale_y_continuous(labels = scales::percent)
)
```

### Order bars

Set the levels of x-axis:

``` r
ggplot(theTable,
       aes(x=reorder(Position,Position,
                     function(x)-length(x)))) +
       geom_bar()
```

or set the limits of x-axis:

``` r
positions <- c("Goalkeeper", "Defense", "Striker")
p <- ggplot(theTable, aes(x = Position)) + scale_x_discrete(limits = positions)
```

## Plot median and interquartile range

Use `stat_summary()`, `median_hilow()` and `fun.args`:[^3][^4]

``` r
p <- (
    ggplot(df, aes(x = age_group, y = count, color = TXNAME))
    + stat_summary(
        fun.data = "median_hilow",
        fun.args=(conf.int = 0.50),
        geom = 'pointrange'
    )
)
```

## Plot curve of a formula

Use `stat_function`:

``` r
k <- function (x) {
    0+1*(1-exp(-0.035*x))
}

p <- (
    ggplot(data = data.frame(x = 0), aes(x))
    + stat_function(fun = k)
    + xlim(0, 200)
)
```

## Plot fitted lines

``` r
x <- 1:10
y <- jitter(x^2)

DF <- data.frame(x, y)

p <- ggplot(DF, aes(x = x, y = y)) + geom_point() +
    stat_smooth(method = 'lm', aes(colour = 'linear'), se = FALSE) +
    stat_smooth(method = 'lm', formula = y ~ poly(x,2), aes(colour = 'polynomial'), se= FALSE) +
    stat_smooth(method = 'nls', formula = y ~ a * log(x) +b, aes(colour = 'logarithmic'), se = FALSE, method.args = list(start = c(a=1,b=1))) +
    stat_smooth(method = 'nls', formula = y ~ a*exp(b *x), aes(colour = 'Exponential'), se = FALSE, method.args = list(start = c(a=1,b=1)))
```

## Axes

`font = 2` make the labels bold.
`par(font.axis = 2)` set the global parameter.[^2]

``` r
boxplot(1:10, yaxt = "n") # suppress y axis
axis(side = 2, font = 2)  # 'side = 2' means y axis
```

## Labels

### Superscript and subscript in labels

Use `bquote`:

``` r
library('ggplot2')

p + labs(title = bquote(Mg^'2+'*': 6 mM, '*K[obs]*': 0.035'))
```

for $$Mg^{2+}: 6 mM, K_{obs}: 0.035$$

## Making inset plots

Use `cowplot`[^5]:

``` r
inset <- (
    ggplot(mpg, aes(drv))
    + geom_bar(fill = "skyblue2", alpha = 0.7) 
    + scale_y_continuous(expand = expand_scale(mult = c(0, 0.05)))
    + theme_minimal_hgrid(11)
)

p_combined <- (
    ggdraw(p + theme_half_open(12))
    + draw_plot(inset, .45, .45, .5, .5)
    + draw_plot_label(
        c("A", "B"),
        c(0, 0.45),
        c(1, 0.95),
        size = 12
    )
)
```

---

## References

[^1] https://stackoverflow.com/questions/15595452/x-axis-in-barplot-in-r

[^2] https://stackoverflow.com/questions/21027990/how-do-i-make-the-y-axis-values-bold-in-r

[^3] https://stackoverflow.com/questions/28436467/interquartile-ranges-in-ggplot2

[^4] https://stackoverflow.com/questions/38272992/conf-int-no-longer-working-in-3-3-using-ggplot2-stat-summary

[^5] https://wilkelab.org/cowplot/articles/drawing_with_on_plots.html

[^6] https://sebastiansauer.github.io/percentage_plot_ggplot2_V2/
