# Join

## Join dataframes in a list

Use `reduce()` in purrr.[^1]

``` r
df <- reduce(a_list, left_join, by = 'id')
```

## Join elements in a vector to a string

Ref[^2]

``` r
paste(vector, collapse = '<sep>')
```

---

## References

[^1] https://stackoverflow.com/questions/8091303/simultaneously-merge-multiple-data-frames-in-a-list

[^2] https://stackoverflow.com/questions/2098368/concatenate-a-vector-of-strings-character
