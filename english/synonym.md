# Synonym

## Delete vs Remove

- Delete 表示删除，强调把对象销毁，对象不存在了。
- Remove 表示移除，把对象从一个集合或容器中移除，但对象仍然存在。

Remove and Delete are defined quite similarly, but the main difference between them is that delete means erase (i.e. rendered nonexistent or nonrecoverable), while remove denotes take away and set aside (but kept in existence).[^1]

## References

[^1] https://www.zhihu.com/question/30657460
