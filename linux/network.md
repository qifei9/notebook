# Network

## NetworkManager

### Using iwd as the Wi-Fi backend

Create `/etc/NetworkManager/conf.d/wifi_backend.conf`:[^1]

``` conf
[device]
wifi.backend=iwd
```

---

## References

[^1] https://wiki.archlinux.org/index.php/NetworkManager
