# Hill equation and Hill plot

The Hill equation and Hill plot are used to measure *Cooperative binding*.

## Cooperative binding

Cooperative binding is a kind of molecular binging, for example, between protein or nucleic acid and its ligand.

A receptor molecule is said to exhibit cooperative binding if its binding to ligand scales nonlinearly with ligand concentration.

Cooperativity can be positive (if binding of a ligand molecule increases the receptor's apparent affinity, and hence increases the chance of another ligand molecule binding) or negative (if binding of a ligand molecule decreases affinity and hence makes binding of other ligand molecules less likely).

The concept of cooperative binding only applies to molecules or complexes with more than one ligand binding site. (**NOTE:** it seems to be applied to different range like ribozyme binging to $$Mg^{2+}$$ which I think having only one binding site)

Cooperativity can be *homotropic*, if a ligand influences the binding of ligands of the same kind, or *heterotropic*, if it influences binding of other kinds of ligands. In the case of hemoglobin, Bohr observed homotropic positive cooperativity (binding of oxygen facilitates binding of more oxygen) and heterotropic negative cooperativity (binding of $$CO_2$$ reduces hemoglobin's facility to bind oxygen).

## Hill equation

The original Hill equation could be change into a form of:

$$ \log{\frac{\bar{Y}}{1-\bar{Y}}} = n*\log{[X]} - \log{K_{d}} $$

where $$\bar{Y}$$ is the *fractional occupancy*, i.e., the fraction of bound sites:

$$ \bar{Y} = \frac{[bound sites]}{[total sites]} $$

**NOTE:** If the molecular is active by ligand binding, it could be:

$$ \bar{Y} = \frac{[active molecular]}{[inactive molecular]} $$

$$[X]$$ is the concentration of the ligand (**NOTE:** like $$Mg^{2+}$$ for ribozyme), $$K_{d}$$ is the apparent dissociation constant (表观解离常数), and $$n$$ is the Hill coefficient.

在适当的情况下，希尔常数的值描述了配体以下列几种方式结合时的协同性：

- $$n > 1$$ --- 正协同反应：一旦一个配体分子结合到酶上，酶对其他配体的亲和力就会增加。

- $$n < 1$$ --- 负协同反应：一旦一个配体分子结合到酶上，酶对其他配体的亲和力就会减小。

- $$n = 1$$ --- 非协同反应：酶对于一个配体分子的亲和力并不取决于是否有配体分子已结合到其上。

## Hill plot

Hill plot is a plot of the Hill equation: $$\log{\frac{\bar{Y}}{1-\bar{Y}}}$$ on the y-axis, $$\log{[X]}$$ on the x-axis, and a linear-fitted line. Thus the slope of the fitted line is the Hill coefficient ($$n$$), and the intercept is the $$\log{K_{d}}$$.

---

## References

[^1] https://doi.org/10.1371/journal.pcbi.1003106
