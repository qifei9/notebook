# AWS Lightsail

## Create an instance

1. 在[官方网站](https://aws.amazon.com/cn/lightsail/)创建实例。第一个月免费。
1. 选择地区--东京
1. 选择镜像Ubuntu 18.04 LTS (Amazon Linux 2018.03.0不支持systemd)
1. 上传SSH key--公钥
1. 点击创建即可完成

## Config

1. 联网-->防火墙，添加相应端口的TCP和UDP规则
1. Enable BBR:[^5]
    ``` bash
    sudo modprobe tcp_bbr
    echo "tcp_bbr" | sudo tee --append /etc/modules-load.d/modules.conf
    echo "net.core.default_qdisc=fq" | sudo tee --append /etc/sysctl.conf
    echo "net.ipv4.tcp_congestion_control=bbr" | sudo tee --append /etc/sysctl.conf
    sudo sysctl -p
    ```

## Setup V2ray

1. ssh连接到服务器 (默认用户名ubuntu)
1. (可选) 设置root和ubuntu账户的密码 (`sudo passwd`, `sudo passwd ubuntu`)
1. (optional) update the system (`sudo apt update`, `sudo apt upgrade`)
1. `su`切换到root账户 (`sudo`运行V2ray的安装脚本会有问题)
1. `bash <(curl -L -s https://install.direct/go.sh)`
1. 编辑`/etc/v2ray/config.json`:
    ``` json
    {
      "inbounds": [{
        "port": 10086, // 服务器监听端口，必须和上面的一样
        "protocol": "vmess",
        "settings": {
          "clients": [{
            "id": "b831381d-6324-4d53-ad4f-8cda48b30811",
            "level": 1,
            "alterId": 64
          }]
        }
      }],
      "outbounds": [{
        "protocol": "freedom",
        "settings": {}
      }]
    }
    ```
    注意更改上面的`port`和`id`。
1. `systemctl start v2ray.service`
1. 编辑**本地的**`/etc/v2ray/config.json`:
    ``` json
    {
      "inbounds": [{
        "port": 1080,  // SOCKS 代理端口，在浏览器中需配置代理并指向这个端口
        "listen": "127.0.0.1",
        "protocol": "socks",
        "settings": {
          "udp": true
        }
      }],
      "outbounds": [{
        "protocol": "vmess",
        "settings": {
          "vnext": [{
            "address": "server", // 服务器地址，请修改为你自己的服务器 ip 或域名
            "port": 10086,  // 服务器端口
            "users": [{
              "id": "b831381d-6324-4d53-ad4f-8cda48b30811",
              "level": 1,
              "alterId": 64
            }]
          }]
        }
      },{
        "protocol": "freedom",
        "tag": "direct",
        "settings": {}
      }],
      "routing": {
        "domainStrategy": "IPOnDemand",
        "rules": [{
          "type": "field",
          "ip": ["geoip:private"],
          "outboundTag": "direct"
        }]
      }
    }
    ```
    注意更改上面的`address`, `port`和`id`。
1. `sudo systemctl start v2ray.service` (local)

## Troubleshooting

### Unable to ping the server

"ping" is disabled by AWS and currently not possible to enable for Lightsail.[^3]

Allow TCP+UDP 0-65535 in the firewall setting may enable ICMP (the protocol required for ping).[^4] *Untested*


---

## References

[^1] https://www.zrj96.com/post-524.html

[^2] https://www.v2ray.com/

[^3] https://forums.aws.amazon.com/thread.jspa?threadID=252152

[^4] https://www.v2ex.com/t/445344

[^5] https://github.com/iMeiji/shadowsocks_install/wiki/%E5%BC%80%E5%90%AF-TCP-BBR-%E6%8B%A5%E5%A1%9E%E6%8E%A7%E5%88%B6%E7%AE%97%E6%B3%95
