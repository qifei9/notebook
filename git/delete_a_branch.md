# Delete a branch

## Local

``` bash
git branch -d <branch>
```

## Remote

``` bash
git push origin --delete <branch>
```

## Delete a local reference to a remote branch

``` bash
git branch -r -d <remote>/<branch>
```

---

## References

[^1] https://www.git-tower.com/learn/git/faq/delete-remote-branch
