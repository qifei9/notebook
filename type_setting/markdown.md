# Markdown

## Escape backtick '\`'

1. Use backticks:[^1]

    ```
    ``foo` `` produces foo`
    `` `foo`` produces `foo
    `` ` `` produces single backtick `
    ```

    `````
    ````
    ```
    Example of Markdown syntax for fenced code block with triple back ticks
    ```
    ````
    `````

    produces

    ``````
    ```
    Example of Markdown syntax for fenced code block with triple back ticks
    ```
    ``````

1. Use backslash[^1]

    Markdown provides backslash escapes for the following characters:

    ```
    \   backslash
    `   backtick
    *   asterisk
    _   underscore
    {}  curly braces
    []  square brackets
    ()  parentheses
    #   hash mark
    +   plus sign
    -   minus sign (hyphen)
    .   dot
    !   exclamation mark
    ```

    for example, this:

    ```
    ## \\ \` \* \_ \{ \} \[ \] \( \) \# \+ \- \. \!
    ```

    returns:

    ```
    \ ` * _ { } [ ] ( ) # + - . !
    ```

## Table

``` markdown
| sample  | batch | end | library          |
|---------|-------|-----|------------------|
| s168-OH | s168  | 3'  | FKDL190743691-1a |
| s168-P  | s168  | 5'  | FKDL190743692-1a |
```

produces

| sample  | batch | end | library          |
|---------|-------|-----|------------------|
| s168-OH | s168  | 3'  | FKDL190743691-1a |
| s168-P  | s168  | 5'  | FKDL190743692-1a |

To edit table for markdown in vim, using plugin [vim-table-mode](https://github.com/dhruvasagar/vim-table-mode).

## References

[^1] https://meta.stackexchange.com/questions/82718/how-do-i-escape-a-backtick-within-in-line-code-in-markdown
