# convert

## covert PDF to image with high resolution

``` bash
convert -density 300 -trim test.pdf -quality 100 test.jpg
```

## Troubleshooting

### convert: operation not allowed by the security policy

Edit `/etc/ImageMagick-7/policy.xml`, add

``` xml
<policy domain="coder" rights="read | write" pattern="PDF" />
```

and delete

``` xml
<policy domain="delegate" rights="none" pattern="gs" />
```

---

## Reference

[^1] https://stackoverflow.com/questions/6605006/convert-pdf-to-image-with-high-resolution

[^2] https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion
