# Tips

## I/O

### read/write csv/tsv

- use csv

    - read

        ``` python
        import csv
        with open('in.csv') as f:
            f_csv = csv.reader(f)
            headers = next(f_csv)
            for row in f_csv:
                ...
        ```

        For tsv, use `csv.reader(f, delimiter='\t')`.

        Read to a dict, and access by column name:

        ``` python
        import csv
        with open('in.csv') as f:
            f_csv = csv.DictReader(f)
            for row in f_csv:
                ...
        ```

    - write

        ``` python
        out = csv.writer(sys.stdout)
        out.writerow(header)
        out.writerows(rows)
        ```

        For data in dict:

        ``` python
        out = csv.DictWriter(sys.stdout, fieldnames=header)
        out.writeheader()
        out.writerows(rows)
        ```

        For the fieldnames:[^3]

        > The fieldnames parameter is a sequence of keys that identify the order in which values in the dictionary passed to the `writerow()` method are written to file.

- use pandas

    Use `pandas.read_csv()`.[^4]


## String manipulation

### Upper- and lowercase a string

`str.upper()` and `str.lower()`.

The other command:

``` python
>>> '2s'.capitalize()
'2s'
>>> 'bob'.title()
'Bob'
>>> 'sandy'.title()
'Sandy'
>>> '1bob'.title()
'1Bob'
>>> '1sandy'.title()
'1Sandy'
>>> '1bob sandy'.title()
'1Bob Sandy'
>>> '1JoeBob'.title()
'1Joebob'

>>> 'hello world'.capitalize()
'Hello world'
>>> 'hello world'.title()
'Hello World'
```

``` python
import string
string.capwords("they're bill's friends from the UK")
>>>"They're Bill's Friends From The Uk"
```

### Split strings

- Use `str.split()`:[^5]

    ``` python
    str.split(sep=None, maxsplit=-1)
    ```

    - `sep`, the character used as delimiter
    - `maxsplit`, the maximum times for split. The list generated after will have maximum `maxsplit + 1` elements.

- Use `re.split`[^7]

    ``` python
    >>> import re
    >>> re.split(r'\s+', 'a b   c')
    ['a', 'b', 'c']
    ```

### Replace

`str.replace()`

``` python
s = '123:456:789'
s.replace(':', '-')
# get '123-456-789'
```


## list manipulation

### initialize a list

#### create a list of 1000 zero

``` python
# 1
arr = [0 for i in range(1000)]

#2 faster
arr = [0]*1000
```

### Count occurrences of items in list

Ref[^6]

- For one item's count, use `count`

    ``` python
    >>> [1, 2, 3, 4, 1, 4, 1].count(1)
    3
    ```

- For occurrence of each item, use `Counter`

    ``` python
    >>> from collections import Counter
    >>> z = ['blue', 'red', 'blue', 'yellow', 'blue', 'red']
    >>> Counter(z)
    Counter({'blue': 3, 'red': 2, 'yellow': 1})
    ```

- Use `value_counts` from `pandas`

    ``` python
    >>> import pandas as pd
    >>> a = [1, 2, 3, 4, 1, 4, 1]
    >>> pd.Series(a).value_counts()
    1    3
    4    2
    3    1
    2    1
    dtype: int64
    ```

### Remove redundant elements from 2 lists

``` python
x = [1,2,3,4]
f = [1,11,22,33,44,3,4]

# 1st
res = list(set(x+f))

# 2nd
result = set(f) - set(x)
```

### Sort a list

`list.sort()` and `sorted(iterable)`.[^9]

> sort 与 sorted 区别：
> 
> sort 是应用在 list 上的方法，sorted 可以对所有可迭代的对象进行排序操作。
> 
> list 的 sort 方法返回的是对已经存在的列表进行操作，无返回值，而内建函数 sorted 方法返回的是一个新的 list，而不是在原来的基础上进行的操作。

### Get index and value in a loop

``` python
for index, item in enumerate(items):
    print(index, item)
```

## Dictionary manipulation

###  Get dictionary keys as a list

[^8]

``` python
newdict = {1:0, 2:0, 3:0}
newdict.keys() # will give dict_keys([1, 2, 3]) not a list

# 1st method
list(newdict.keys())

# 2nd method
list(newdict)

# 3rd method
[*newdict]

# 4th
[k for k in newdict.keys()]
# or the same
[k for k in newdict]

# 5th
*k,  = newdict

# to get keys as a tuple or set
*newdict # gives (1, 2, 3) a tuple
{*newdict} # gives {1, 2, 3} a set
```


### defaultdict

``` python
from collections import defaultdict

d = defaultdict(list)
```

### Get dictionary values for list of keys

Ref[^1]

- list comprehension

    ``` python
    >>> [mydict[x] for x in mykeys]
    [3, 1]
    ```

- other ways

    - Build list and throw exception if key not found: `map(mydict.__getitem__, mykeys)`
    - Build list with `None` if key not found: `map(mydict.get, mykeys)`
    - Alternatively, using `operator.itemgetter` can return a tuple:

    ``` python
    from operator import itemgetter
    myvalues = itemgetter(*mykeys)(mydict)
    # use `list(...)` if list is required
    ```

*Note*: in Python3, `map` returns an iterator rather than a list. Use `list(map(...))` for a list.

### Check if a given key already exists in a dictionary

Use `in`:[^2]

``` python
d = dict()

if 'key1' in d:
  print "blah"
else:
  print "boo"
```

---


## Errors

### Raise an error and exit

``` python
raise SystemExit('Error: error message')
```

## References

[^1] https://stackoverflow.com/questions/18453566/python-dictionary-get-list-of-values-for-list-of-keys

[^2] https://stackoverflow.com/questions/1602934/check-if-a-given-key-already-exists-in-a-dictionary

[^3] https://docs.python.org/3/library/csv.html

[^4] https://python3-cookbook-personal.readthedocs.io/zh_CN/latest/c06/p01_read_write_csv_data.html

[^5] https://docs.python.org/3/library/stdtypes.html#str.split

[^6] https://stackoverflow.com/questions/2600191/how-can-i-count-the-occurrences-of-a-list-item

[^7] https://www.liaoxuefeng.com/wiki/1016959663602400/1017639890281664

[^8] https://stackoverflow.com/questions/16819222/how-to-return-dictionary-keys-as-a-list-in-python

[^9] https://www.runoob.com/python/python-func-sorted.html
