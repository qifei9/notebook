# Rebase

## Rebase a branch onto the top of master

Assume the following history exists and the current branch is "topic":

```
      A---B---C topic
     /
D---E---F---G master
```

From this point, the result of **either** of the following command:

``` bash
git rebase master
```

``` bash
git rebase master topic
```

would be:

```
              A'--B'--C' topic
             /
D---E---F---G master
```

## References

[^1] https://git-scm.com/docs/git-rebase
