# unique

## Keep elements that appear only once

[^1]

``` R
df[!(duplicated(df) | duplicated(df, fromLast = TRUE)), ]
```

---

## References

[^1] https://stackoverflow.com/questions/13763216/how-can-i-remove-all-duplicates-so-that-none-are-left-in-a-data-frame
