# Plugins

## VIM Table Mode

[vim-table-mode](https://github.com/dhruvasagar/vim-table-mode), used for editing table in markdown.

### Usage

1. `<Leader>tm` to toggle table-edit-mode.
1. The plugin will automatically adjust the formatting to match the text you’re entering every time you press `|`.
1. In the second line (without leaving Insert mode), enter `|` twice, the plugin will write a properly formatted horizontal line.

## Nvim-R

### Config

1. Change R path

  `let R_path = /path/to/R` will append the path to `$PATH`, but the plugin still run the first R founded in `$PATH`. Therefore, if you already have a R in `$PATH`, but you want the plugin to run another R, this will not work. Note, the `/path/to/R` has to be a dir.

  `let R_app = command` will make the plugin run the `command`. This can be set to a path to R or a script invoking R. This will make the plugin ignore the R already exits in `$PATH`.

### Shortcut

- Line (evaluate and insert the output as comment)    `\o`
