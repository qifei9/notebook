# ggplot2

## Legend

- set legend position

    ``` r
    # position
    theme(legend.position = "bottom")
    # coordinate
    theme(legend.position = c(.2,.85))
    # disable legend
    theme(legend.position = "none")
    ```

- set legend text

    ``` r
    # size and type
    p + guides(
        fill = guide_legend(
            title.theme = element_text(size = 13, face = 2),
            label.theme = element_text(size = 25, face = 1),
            label.hjust = 0
        )
    )

    # use expression

    p + scale_fill_manual(
        values = rev(pal_npg('nrc', 0.8)(3)),
        name = '',
        labels = expression('XRN-1'^'-'*' 1', 'XRN-1'^'-'*' 2', 'XRN-1'^'+')
    )
    ```

- set legend size

    ``` r
    theme(
        legend.key = element_rect(size = 5),
        legend.key.size = unit(1.5, 'lines'),
    )
    ```

- change legend title

    ``` r
    # Modify legend titles
    p + labs(fill = "Dose (mg)")
    ```

## Facet

- set different scales for facet

    ``` r
    devtools::install_github("zeehio/facetscales")

    library(ggplot2)
    library(scales)
    library(facetscales)
    mydf <- data.frame(
      Subject = rep(c("A", "B", "C", "D"), each = 3),
      Magnitude = rep(c("SomeValue", "Percent", "Scientific"), times = 4),
      Value = c(c(170,0.6,2.7E-4),
                c(180, 0.8, 2.5E-4),
                c(160, 0.71, 3.2E-4),
                c(159, 0.62, 3E-4)))

    scales_y <- list(
      Percent = scale_y_continuous(labels = percent_format()),
      SomeValue = scale_y_continuous(),
      Scientific = scale_y_continuous(labels = scientific_format())
    )

    ggplot(mydf) +
      geom_point(aes(x = Subject, y = Value)) +
      facet_grid_sc(rows = vars(Magnitude), scales = list(y = scales_y, x = 'free'))
    ```

- set labels for facet

    ``` r
    # display value or name and value
    p + facet_grid(rows = vars(Rep), cols = vars(rz), labeller = labeller(.rows = label_both, .cols = label_value))
    
    # use named vector of strings as label
    conservation_status <- c(
      cd = "Conservation Dependent",
      en = "Endangered",
      lc = "Least concern",
      nt = "Near Threatened",
      vu = "Vulnerable",
      domesticated = "Domesticated"
    )

    p + facet_grid(vore ~ conservation, labeller = labeller(conservation = conservation_status))

    # change the text
    p + theme(
        strip.text.x = element_text(size = rel(1.5), face = 2),
        strip.text.y = element_text(size = rel(1.3), face = 2)
    )
    ```

- set facet background

    ``` r
    theme(strip.background = element_rect(fill = "white"))
    ```

- remove border of facet title

    ``` r
    theme(strip.background = element_blank())
    ```

## Axis

- set axis ticks

    ``` r
    theme(axis.ticks.length = unit(0.30, "cm"))
    ```

- set expand of axis

    ``` r
    # for barplot
    scale_y_continuous(expand = expand_scale(mult = c(0, 0.05)))
    ```

## Label

- use math characters in labels

    ``` r
    scale_fill_manual(
        labels = c(
            bquote('XRN-1('-') 1'),
            bquote('XRN-1('-') 2'),
            bquote('XRN-1('+')')
        )
    )

    labs(x = 'Time (min)', y = bquote(italic(Delta*F)))

    annotate(
        'text',
        x = 65,
        y = 0.1,
        label = "paste(italic(k)[obs] == 0.0432, \" ± \", 0.0065, \" \", min^-1)",
        parse = T,
        size = 20
    )
    ```

## Border

- add border to panel and plot

    ``` r
    plot + theme(
        panel.background = element_rect(color = "white"), # panel border
        plot.background  = element_rect(color = "grey90") # plot border
    )
    ```

## Annotation

- add text to heatmap

    ``` r
    p <- (
        ggplot(data, aes(chr_a, chr_b))
        + geom_raster(aes(fill = num))
        + geom_text(aes(label = num)) # adding text to tile
        + scale_fill_distiller(palette = "YlGnBu", direction = 1)
    )
    ```

- add text (like count, percentage) to barplot

    ``` r
    p_same_chr <- (
        ggplot(rpc_count_region, aes(same_chr))
        + geom_bar(aes(fill = same_chr))
        # adding count
        + geom_text(stat = 'count', aes(y = ..count.., label = ..count..))
        # adding percentage
        + geom_text(
            stat = "count",
            aes(y = (..count..)/sum(..count..),
                label = scales::percent((..count..)/sum(..count..))),
            vjust = -0.25
        )
    )
    ```

## Scale

- scale size of geom_point

    ``` r
    scale_size(range = c(1,15))
    ```

- multiple color/fill scales

    Use [ggnewscale](https://eliocamp.github.io/ggnewscale/).

## P-value

- add p-values to plot

    ``` r
    library('ggpubr')
    
    p <- (
        p + stat_compare_means(
            comparisons = list(c("h", "g")),
            method = "wilcox.test",
            size = 8,
            label = 'p.format'
        )
    )
    ```

## Color

- diverging color

    ``` r
    p + scale_fill_gradient2(high = 'red', low = "blue", mid = "white", midpoint = 0)
    ```
