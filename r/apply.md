# apply

## lapply

``` R
lapply(X, FUN, ...)
```

When `X` is a vector, `lapply` will return a list. `...` are parameters for `FUN`.

## sapply

``` R
sapply(X, FUN, ..., simplify = TRUE, USE.NAMES = TRUE)
```

`sapply` is a wrapper of `lapply`. When `X` is a vector, it returns a vector. When `USE.NAMES = T`, the values of `X` will be used as names of the returned vector.
