# Optimize

## SD card

For class 10 card, edit `/boot/config.txt`, add

```
dtparam=sd_overclock=100
```

## f2fs

For Archlinux Arm, format the root partition as f2fs format when install.[^1]

For Raspbian, see[^2] (untested).

## Fan

Adding a fan to rpi maybe a bad idea.[^3]

Tested on Raspbian, when playing video with chromium, the CPU is about 58 degree without fan.

---

## References

[^1] https://www.zybuluo.com/yangxuan/note/344907

[^2] http://whitehorseplanet.org/gate/topics/documentation/public/howto_ext4_to_f2fs_root_partition_raspi.html

[^3] https://www.raspberrypi.org/forums/viewtopic.php?t=194621
