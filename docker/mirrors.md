# Mirrors

## List

- quay.io
    - http://quay.mirrors.ustc.edu.cn/ [^1]
- Docker Hub
    - https://dockerhub.azk8s.cn
    - https://hub-mirror.c.163.com

## How to use

### In command line

``` bash
docker pull quay.mirrors.ustc.edu.cn/bcbio/bcbio-rnaseq:latest
```

### Add to config

`/etc/docker/daemon.json`:

``` json
{
  "registry-mirrors": [
    "https://dockerhub.azk8s.cn",
    "https://hub-mirror.c.163.com"
  ]
}
```

---

## References

[^1] https://servers.ustclug.org/2017/10/ustc-mirrors-add-new-repo/
