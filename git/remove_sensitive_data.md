# Remove sensitive data from git repo

## Using BFG

``` bash
bfg --delete-files YOUR-FILE-WITH-SENSITIVE-DATA

bfg --replace-text passwords.txt
```

### Using filter-branch

``` bash
# rewrite the history
git filter-branch --force --index-filter \
'git rm --cached --ignore-unmatch PATH-TO-YOUR-FILE-WITH-SENSITIVE-DATA' \
--prune-empty --tag-name-filter cat -- --all

# add to .gitignore
echo "YOUR-FILE-WITH-SENSITIVE-DATA" >> .gitignore
git add .gitignore
git commit -m "Add YOUR-FILE-WITH-SENSITIVE-DATA to .gitignore"

# overwrite the remote
git push origin --force --all
git push origin --force --tags

# make sure everything is OK, then prune
git for-each-ref --format='delete %(refname)' refs/original | git update-ref --stdin
git reflog expire --expire=now --all
git gc --prune=now
```

## References

[^1] https://help.github.com/articles/removing-sensitive-data-from-a-repository/
