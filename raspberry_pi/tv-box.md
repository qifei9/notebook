# TV-box

> Setup a rpi to use as a TV-box.

## Choose the system

Use Rasbian. It's optimized for rpi, shipped with a well designed desktop and also a good out-box experience of hardware-accelerated video decoding.

Archlinux Arm does not have that well designed desktop by default and is hard to get hardware-accelerated video decoding.

For OSMC, although the system itself performs well, there are not many addons working for Chinese video website. The only working addons are bilibili2, QIYI, Pandatv and Yinyuetai from [xbmc-addons-chinese](https://github.com/taxigps/xbmc-addons-chinese).

## Hardware-accelerated video decoding

1. For graphic driver, use the legacy driver rather than the VC4/V3D driver. The later one has no hardware video decoding support yet (2019-01-22).[^1]

1. For player and browser, use VLC and Chromium from the default repo. They are patched version and support hardware video decoding.[^1]

## UPnP client

TODO

---

## References

[^1] https://www.phoronix.com/forums/forum/software/distributions/1061575-raspbian-2018-11-13-brings-hardware-accelerated-vlc-media-player
