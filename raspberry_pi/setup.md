# Setup

> For Raspberry Pi 3 Model B+

## Arch Linux ARM

### Install

Follow the instruction on [official website](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3). Note:

- Use AArch64 installation (**AArch64 do not have kodi yet!!**).
- After step 2, pull out the stick/TF card and plug in again.
- Login as root.
- Step 10 has to be run as root.

### Connect to internet

Run `dhcpcd eth0` to enable wired LAN, `wifi-menu` to connet to wifi.

To auto enable them on next boot, run:

``` bash
pacman -S wpa_actiond ifplugd
systemctl enable netctl-ifplugd@eth0
systemctl enable netctl-auto@waln0
```

### Set pacman mirror

1. (optional) Install vim by `pacman -S vim` (currently neovim is unavailable for AArch64 on rpi3).
1. Add TUNA archlinuxarm mirror to `/etc/pacman.d/mirrorlist`, and commit out the default mirror.

    ```
    Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxarm/$arch/$repo
    ```

1. Update system `pacman -Syu`.

### Manager users

1. Change root password.

    ``` bash
    passwd root
    ```

1. Add user and set password.

    ``` bash
    useradd -m <user>
    passwd <user>
    ```

1. Delete default user 'alarm' (may need reboot befrom this).

    ``` bash
    userdel -r alarm
    ```

1. Add new user to sudo group.
    1. Install `sudo` by `pacman -S sudo`.
    2. Run `visudo`, and add `<USER_NAME>   ALL=(ALL) ALL`.

### Edit hostname

1. Edit `/etc/hostname`.
1. Edit `/etc/hosts`, adding:

    ```
    127.0.0.1   localhost
    ::1         localhost
    127.0.1.1   myhostname.localdomain	myhostname
    ```

## Raspbian

### Install

1. Download the image from the [official website](https://www.raspberrypi.org/downloads/raspbian/).

1. Write the image to the SD card:

    ``` bash
    unzip -p 2018-11-13-raspbian-stretch-full.zip | sudo dd of=/dev/sdx bs=4M status=progress conv=fsync
    ```

    Change the `of=/dev/sdx` to the SD card.

1. Insert the SD card to rpi, boot and then follow the wizard.

### Set apt mirror

1. Edit `/etc/apt/sources.list`, commit out original mirrors and add:

    ```
    deb https://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ stretch main contrib non-free rpi
    # deb-src https://mirrors.tuna.tsinghua.edu.cn/raspbian/raspbian/ stretch main contrib non-free rpi
    ```

1. Edit `/etc/apt/sources.list.d/raspi.list`, commit out original mirrors and add:

    ```

    deb https://mirror.tuna.tsinghua.edu.cn/raspberrypi/ stretch main ui
    # deb-src https://mirror.tuna.tsinghua.edu.cn/raspberrypi/ stretch main ui
    ```

1. Update system:

    ``` bash
    sudo apt update
    sudo apt upgrade
    ```

### Config

1. Enable root account (default user pi can run `sudo` without password):

    ``` bash
    sudo passwd root
    ```
