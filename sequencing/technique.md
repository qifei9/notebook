# Technique

## Helicos

1. Single molecule fluorescent sequencing.
1. Sequencing by Synthesis (SBS).
1. Slow, and thus low-throughput (because the process is halted between each extension step).[^1]
1. Read length is short (the read lengths realized are 32 nucleotides long).[^1]
1. The error rate is high due to noise.[^1]
1. The machine is more expensive than the other techniques (less than 10 machines sold).[^2]
1. This technique is already deprecated and the company, Helicos Biosciences, went bankrupt on Nov 2012.[^3]
1. The technique is taken by Direct Genomics (瀚海基因, founded by Jiankui He 贺建奎). The machine is called GenoCare.

---

## References

[^1] https://en.wikipedia.org/wiki/Helicos_single_molecule_fluorescent_sequencing

[^2] https://www.zhihu.com/question/63333982/answer/373943991

[^3] https://en.wikipedia.org/wiki/Helicos_Biosciences
