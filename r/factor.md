# Factor

## Add a level to factor

``` R
levels(f1) <- c(levels(f1),"new_level")
```

## Order factor by another variable

``` r
fct_reorder(Species, Sepal.Width, .desc = TRUE)

fct_reorder2(Chick, Time, weight)
```

## Modify the level

``` r
f <- factor(c("a", "b", "c", "d"), levels = c("b", "c", "d", "a"))
fct_relevel(f)
#> [1] a b c d
#> Levels: b c d a
fct_relevel(f, "a")
#> [1] a b c d
#> Levels: a b c d
fct_relevel(f, "b", "a")
#> [1] a b c d
#> Levels: b a c d

# Move to the third position
fct_relevel(f, "a", after = 2)
#> [1] a b c d
#> Levels: b c a d

# Relevel to the end
fct_relevel(f, "a", after = Inf)
#> [1] a b c d
#> Levels: b c d a
fct_relevel(f, "a", after = 3)
#> [1] a b c d
#> Levels: b c d a

# Revel with a function
fct_relevel(f, sort)
#> [1] a b c d
#> Levels: a b c d
fct_relevel(f, sample)
#> [1] a b c d
#> Levels: a c d b
fct_relevel(f, rev)
#> [1] a b c d
#> Levels: a d c b
```
