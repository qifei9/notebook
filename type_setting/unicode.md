# Unicode

## Input Unicode character

### Vim

1. `i` go into INSERT mode
1. `Ctrl+v` go into ins-special-keys mode
1. `u2713` insert the Unicode character `CHECK MARK (U+2713)`

### Word

1. type the character code
1. press `ALT+X`


## Unicode character list

| Name       | Chinese Name | Code     | Character | Usage                      |
|------------|--------------|----------|-----------|----------------------------|
| Apostrophe | 撇号         | `U+0027` | `'`       | China's foreign policy     |
| Hyphen     | 连字符       | `U+2010` | `‐`       | inter-word                 |
| En-dash    | En 长划      | `U+2013` | `–`       | page range, 1–10           |
| Em-dash    | Em 长划      | `U+2014` | `—`       | punctuation dash—like this |
| Minus Sign | 负号         | `U+2212` | `−`       | 0, 1 and −1                |
