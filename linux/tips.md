# Tips

## Convert EOL of files

Use `dos2unix`, `unix2dos` and `unix2mac`.

``` bash
yay -S dos2unix
```
