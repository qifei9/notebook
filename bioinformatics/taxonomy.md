# Taxonomy

Use [taxize](https://github.com/ropensci/taxize) ([manual](https://taxize.dev/)).

``` r
uid_aln <- get_uid(sp_aln$sn)

cl_aln <- classification(uid_aln, db = 'ncbi')

class_aln <- sapply(cl_aln, function(x) x[x$rank %in% c('class'), 'name']) %>%
    plyr::ldply(as_tibble, .id = 'taxID') %>%
    rename(class = value) %>%
    unique()

order_aln <- sapply(cl_aln, function(x) x[x$rank %in% c('order'), 'name']) %>%
    plyr::ldply(as_tibble, .id = 'taxID') %>%
    rename(order = value) %>%
    unique()

species_aln <- sapply(
        cl_aln,
        function(x) x[x$rank %in% c('species'), 'name']
    ) %>%
    plyr::ldply(as_tibble, .id = 'taxID') %>%
    rename(species = value) %>%
    unique()
```
