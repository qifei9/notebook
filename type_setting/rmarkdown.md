# Rmarkdown

## Figure

### Arrange combined figures

Use `fig.ncol` to set number of columns for combined figures:[^1]

```` R
``` {r deg-expression-profiles, echo = F, fig.cap = '(ref:cap-deg-expression-profiles)', fig.subcap = '', out.width = '50%', fig.ncol = 2, fig.pos = 'htbp'}
knitr::include_graphics(c("../../result/DE_analysis/heatmap_deg_relative_CvsH.pdf", "../../result/DE_analysis/heatmap_deg_relative_PCvsH.pdf", "../../result/DE_analysis/heatmap_deg_relative_CvsPC.pdf"))
```
````

## References

[^1] https://github.com/yihui/knitr/issues/1327
