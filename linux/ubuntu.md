# ubuntu

## apt-get run to error 'E:Encountered a section with no Package: header'

Error:

``` bash
E:Encountered a section with no Package: header
E: Problem with MergeList /var/lib/apt/lists/cn.archive.ubuntu.com_ubuntu_dists_trusty_multiverse_i18n_Translation-zh
```

Solution:[^1]

``` bash
$ sudo rm /var/lib/apt/lists/* -vf
$ sudo apt-get update
```

## Install openjdk-11 for ubuntu 14.04

Command:[^2]

``` bash
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install openjdk-8-jdk
sudo update-alternatives --config java
sudo update-alternatives --config javac
```

---

## References

[^1] https://www.zybuluo.com/natsumi/note/200114

[^2] https://askubuntu.com/a/666481
