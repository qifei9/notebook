# ibus

## Installation

``` bash
yay -S ibus ibus-libpinyin ibus-qt
```

## Setup

``` bash
ibus-setup
```

For Qt

``` bash
qtconfig-qt4
```

In *Interface > Default Input Method*, select *ibus* instead of *xim*. 

In GNOME, to enable input in your language, add it to the *Input Sources* section of the *Region & Language* settings.

---

## References

[^1] https://wiki.archlinux.org/index.php/IBus
