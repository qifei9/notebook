# submodules

## Get submodules

``` sh
# git clone
git clone --recursive
# or
git clone --recurse-submodules

# git pull
git pull --recurse-submodules
```

---

## References

[^1] https://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules
