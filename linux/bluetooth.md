# Bluetooth

## Headset

### Connect and config

> It seems the following issue has disappeared (2020-02-01, with pulseaudio version 13.0-3).


> **Do not connect using GNOME Bluetooth!**
>
> The A2DP profile will not activate if connected using GNOME Bluetooth with pulseaudio 9/10 due to an ongoing bug, leading to possible low quality mono sound.

1. Install `pulseaudio-alsa`, `pulseaudio-bluetooth`, `bluez`, `bluez-libs`, `bluez-utils`.

1. Pair and connect:

    ```
    $ bluetoothctl
    [bluetooth]# power on
    [bluetooth]# agent on
    [bluetooth]# default-agent
    [bluetooth]# scan on
    ...
    [bluetooth]# pair <MAC-address>
    [bluetooth]# connect <MAC-address>
    ```

    If get error `org.bluez.Error.Failed`, do not exit the bluetoothctl, open another terminal and kill existing PulseAudio daemon by `pulseaudio -k`.

1. Setting up auto connection:

    1. Edit `/etc/pulse/default.pa`, add:

        ``` conf
        # automatically switch to newly-connected devices
        load-module module-switch-on-connect
        ```

    1. Inside bluetoothctl console, run `[bluetooth]# trust <MAC-address>`.

    1. (optional) Auto enable bluetooth adapter. Edit `/etc/bluetooth/main.conf`, add to the `[Policy]` section:

        ``` conf
        AutoEnable=true
        ```

### Troubleshooting

#### Gnome with GDM

GDM will run its own pulseaudio instance, which "captures" your bluetooth device connection and makes the device unavailable, and it may also leads to PulseAudio fails when changing the profile to A2DP. To prevent GDM from starting its own instance of PulseAudio: 

1. Prevent Pulseaudio clients from automatically starting a server if one is not running by adding the following to `/var/lib/gdm/.config/pulse/client.conf`:

    ``` conf
    autospawn = no
    daemon-binary = /bin/true
    ```

1. Give access to this file to `gdm` user:

    ``` bash
    $ sudo chown gdm:gdm /var/lib/gdm/.config/pulse/client.conf
    ```

1. Prevent systemd from starting Pulseaudio anyway with socket activation:

    ``` bash
    $ sudo -ugdm mkdir -p /var/lib/gdm/.config/systemd/user
    $ sudo -ugdm ln -s /dev/null /var/lib/gdm/.config/systemd/user/pulseaudio.socket
    ```

    **NOTE**: It seems the above steps could not prevent `gdm` to run pulseaudio (2020-02-03).

1. Restart or just kill pulseaudio by `sudo killall pulseaudio`, and check that there is no Pulseaudio process for the `gdm` user:

    ``` bash
    ps aux | grep pulse
    ```

1. Check whether A2DP is available:

    ``` bash
    pactl list | grep -C2 A2DP
    ```

---

## References

[^1] https://wiki.archlinux.org/index.php/Bluetooth_headset

[^2] https://www.debuntu.org/how-to-disable-pulseaudio-and-sound-in-gdm/
