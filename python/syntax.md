# Syntax

## argparse

Ref[^2] [^3] [^4]

## String formatting

Ref[^1]

``` python
>>> errno = 50159747054
>>> name = 'Bob'
```

1. Old style (% operator)

    ``` python
    >>> 'Hey %s, there is a 0x%x error!' % (name, errno)
    # 'Hey Bob, there is a 0xbadc0ffee error!'

    >>> 'Hey %(name)s, there is a 0x%(errno)x error!' % {
    ...     "name": name, "errno": errno }
    # 'Hey Bob, there is a 0xbadc0ffee error!'
    ```

1. New style (`str.format`)

    ``` python
    >>> 'Hello, {}'.format(name)
    # 'Hello, Bob'

    >>> 'Hey {name}, there is a 0x{errno:x} error!'.format(
    ...     name=name, errno=errno)
    # 'Hey Bob, there is a 0xbadc0ffee error!'
    ```

1. String Interpolation / f-Strings (Python 3.6+)

    ``` python
    >>> f'Hello, {name}!'
    # 'Hello, Bob!'

    >>> f"Hey {name}, there's a {errno:#x} error!"
    # "Hey Bob, there's a 0xbadc0ffee error!"
    ```

    ``` python
    >>> a = 5
    >>> b = 10
    >>> f'Five plus ten is {a + b} and not {2 * (a + b)}.'
    # 'Five plus ten is 15 and not 30.'
    ```

    ``` python
    >>> def greet(name, question):
    ...     return f"Hello, {name}! How's it {question}?"
    ...
    >>> greet('Bob', 'going')
    # "Hello, Bob! How's it going?"
    ```

1. Template Strings (Standard Library)

    ``` python
    >>> from string import Template
    >>> t = Template('Hey, $name!')
    >>> t.substitute(name=name)
    # 'Hey, Bob!'

    >>> templ_string = 'Hey $name, there is a $error error!'
    >>> Template(templ_string).substitute(
    ...     name=name, error=hex(errno))
    # 'Hey Bob, there is a 0xbadc0ffee error!'
    ```

> Python String Formatting Rule of Thumb: If your format strings are user-supplied, use Template Strings (#4) to avoid security issues. Otherwise, use Literal String Interpolation/f-Strings (#3) if you're on Python 3.6+, and "New Style" str.format (#2) if you're not.

---

## References

[^1] https://realpython.com/python-string-formatting/

[^2] https://vra.github.io/2017/12/02/argparse-usage/

[^3] https://zhuanlan.zhihu.com/p/34395749

[^4] https://www.jianshu.com/p/fef2d215b91d
