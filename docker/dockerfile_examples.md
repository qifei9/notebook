# Dockerfile examples 

## CODA

- use `python` image as base.
- could not use alpine-based images, since the `split` in alpine cannot be used in CODA.
- change the repository to use TUNA mirror before install any packages (for local build only).

### Examples

#### \#1

- debian (python) based
- autobuild on Docker Hub

``` dockerfile
FROM python:2
MAINTAINER Fei Qi (qifei9@gmail.com)

ENV SAMTOOLS_VERSION=1.10 SEQPREQ_VERSION=1.3.2

WORKDIR /opt

COPY requirements.txt ./CODA/

# install CODA deps
RUN CODADeps='pigz openjdk-11-jre-headless' \
    && apt-get update \
    && apt-get install -y $CODADeps \
    && pip install --no-cache-dir -r ./CODA/requirements.txt \
# install samtools
    && wget https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && tar xjf samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && cd samtools-${SAMTOOLS_VERSION}/ && make \
    && mv samtools /usr/local/bin/ \
    && cd ../ \
    && rm -rf samtools-${SAMTOOLS_VERSION} \
# install SeqPrep
    && wget https://github.com/jstjohn/SeqPrep/archive/v${SEQPREQ_VERSION}.tar.gz \
    && tar xzf v${SEQPREQ_VERSION}.tar.gz \
    && cd SeqPrep-${SEQPREQ_VERSION}/ && make \
    && mv SeqPrep /usr/local/bin \
    && cd ../ \
    && rm -rf SeqPrep-${SEQPREQ_VERSION} \
# clean
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

COPY . ./CODA/

CMD ["/bin/bash"]
```

#### \#2

- debian-based
- local build
- python modules missed

``` dockerfile
FROM python:2
MAINTAINER Fei Qi (qifei9@gmail.com)

ENV SAMTOOLS_VERSION=1.10 SEQPREQ_VERSION=1.3.2

WORKDIR /opt

COPY samtools-${SAMTOOLS_VERSION}.tar.bz2 ./
COPY SeqPrep-${SEQPREQ_VERSION}.tar.gz ./

RUN CODADeps='pigz openjdk-11-jre-headless' \
    && echo "deb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster main contrib non-free\ndeb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-updates main contrib non-free\ndeb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-backports main contrib non-free\ndeb https://mirrors.tuna.tsinghua.edu.cn/debian-security buster/updates main contrib non-free" > /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y $CODADeps \
    && tar xjf samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && cd samtools-${SAMTOOLS_VERSION}/ && make \
    && cd ../ \
    && mv samtools-${SAMTOOLS_VERSION}/samtools /usr/local/bin/ \
    && tar xzf SeqPrep-${SEQPREQ_VERSION}.tar.gz \
    && cd SeqPrep-${SEQPREQ_VERSION}/ && make \
    && cd ../ \
    && mv SeqPrep-${SEQPREQ_VERSION}/SeqPrep /usr/local/bin \
    && rm -rf /opt \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean
CMD ["/bin/bash"]
```

#### \#3

- alpine-based
- unusable
- local build
- python modules missed

``` dockerfile
FROM python:2.7.18-alpine3.11
MAINTAINER Fei Qi (qifei9@gmail.com)

ENV SAMTOOLS_VERSION=1.10 SEQPREQ_VERSION=1.3.2

WORKDIR /opt

COPY samtools-${SAMTOOLS_VERSION}.tar.bz2 ./
COPY SeqPrep-${SEQPREQ_VERSION}.tar.gz ./

RUN buildDeps='build-base zlib-dev' \
    && samtoolsDeps='bzip2-dev xz-dev ncurses-dev curl-dev' \
    && CODADeps='pigz openjdk11' \
    && sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories \
    && apk update \
    && apk add --no-cache $buildDeps \
    && apk add --no-cache $samtoolsDeps \
    && apk add --no-cache $CODADeps \
    && tar xjvf samtools-${SAMTOOLS_VERSION}.tar.bz2 \
    && cd samtools-${SAMTOOLS_VERSION}/ && make \
    && cd ../ \
    && mv samtools-${SAMTOOLS_VERSION}/samtools /usr/bin/ \
    && tar xzvf SeqPrep-${SEQPREQ_VERSION}.tar.gz \
    && cd SeqPrep-${SEQPREQ_VERSION}/ && make \
    && cd ../ \
    && mv SeqPrep-${SEQPREQ_VERSION}/SeqPrep /usr/bin \
    && rm -rf /opt \
    && apk del $buildDeps
CMD ["/bin/sh"]
```
