# Proxy

## Download the image

1. `mkdir /etc/systemd/system/docker.service.d` [^1]
1. add to file `http-proxy.conf`

    ``` conf
    [Service]
    Environment="ALL_PROXY=socks5://localhost:1080"  # no socks5h  
    ```

1. `systemctl daemon-reload`
1. to test 

    ``` bash
    systemctl show --property=Environment docker
    # output Environment=HTTP_PROXY=socks5://localhost:1080
    ```

1. `systemctl restart docker`

---

## References

[^1] https://blog.vinkdong.com/docker%E4%BD%BF%E7%94%A8socks-proxydocker%E4%BD%BF%E7%94%A8shadowsocks%E4%BB%A3%E7%90%86/
