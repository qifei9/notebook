# Save

## Save an object

### Using `save()`

`save()` can save multiple objects in a single call. When `load()` the original name of the object is restored.

``` R
> save(mod, file = "mymodel.rda")
> ls()
[1] "mod"
> load(file = "mymodel.rda")
> ls()
[1] "mod"
```

### Using `saveRDS()`

`saveRDS()` only works for single object. When `readRDS()`, the object could be read in with different name.

``` R
> ls()
[1] "mod"
> saveRDS(mod, "mymodel.rds")
> mod2 <- readRDS("mymodel.rds")
> ls()
[1] "mod"  "mod2"
> identical(mod, mod2, ignore.environment = TRUE)
[1] TRUE
```

## References

[^1] https://www.fromthebottomoftheheap.net/2012/04/01/saving-and-loading-r-objects/
