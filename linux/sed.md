# sed

## Delete lines from a file

Delete lines 2--4:

``` bash
sed '2,4 d' -i <file>
```

Delete lines 2--4 and 10:[^1]

``` bash
sed '2,4d;10d' -i <file>
```

The default is to print to the `STDOUT`. Use `-i` to modify the file in place.

## Edit file

``` bash
sed 's/:.*//' input
```

---

## References

[^1] http://blog.51cto.com/addam/1386331
