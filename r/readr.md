# readr

## Set col types when read in

``` R
bedgraph <- read_tsv(file, col_types = 'fiii', col_names = c('chrom', 'start', 'end', 'value'))
```

`col_types` set the types for columns.

> c = character, i = integer, n = number, d = double, l = logical, f = factor, D = date, T = date time, t = time, ? = guess, or ‘_’/‘-’ to skip the column.
