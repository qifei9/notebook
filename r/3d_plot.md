# 3D plot

## Use plot3D

``` R
library('plot3D')

scatter3D(pca3d[, 1], pca3d[, 2], pca3d[, 3],
          colvar = as.integer(pca3d$health), col = ggcols,
          bty = 'g', pch = 19, cex = 1.5,
          theta = -40, phi = 20,
          xlab = colnames(pca3d)[1],
          ylab = colnames(pca3d)[2],
          zlab = colnames(pca3d)[3],
          colkey = list(at = c(1, 2, 3), side = 1,
                        length = 0.5, width = 0.5,
                        labels = levels(pca3d$health)))
```

- `pca3d[, 1]`, `pca3d[, 2]` and `pca3d[, 3]` are data for the 3 axes.
- `colvar` is the factor for coloring the points.
- `col` is the color palette.
- `bty` is the background.
- `theta` and `phi` are the angles for viewing the plot.
- `colkey` is the legend for color.

### Interactive

Use plot3Drgl.

``` R
# Create his3D using plot3D
hist3D_fancy(iris$Sepal.Length, iris$Petal.Width, colvar=as.numeric(iris$Species))
# Make the rgl version
library("plot3Drgl")
plotrgl()
```

> The package rgl allows to interactively rotate, zoom the graphs. However it’s not yet possible to plot a colorkey (R software (ver. 3.1.2) and plot3D (ver. 1.0-2)).

> Note that, after creating the rgl plot you can use the functions below:
>
>    croprgl(xlim, ylim, zlim, …) to modify the ranges
>
>    cutrgl(…) to zoom in on a selected region of the plot. The current plot will be overwritten
>
>    uncutrgl(…) and uncroprgl(…) restore the original plot.
>
>    …: any arguments for par3d, open3d or material3d in rgl package.


## Use plotly

``` R
set.seed(417)
library(plotly)
temp <- rnorm(100, mean=30, sd=5)
pressure <- rnorm(100)
dtime <- 1:100

plot_ly(x=temp, y=pressure, z=dtime, type="scatter3d", mode="markers", color=temp)
```

## Use gg3D

``` R
devtools::install_github("AckerDWM/gg3D")

library("gg3D")

## An empty plot with 3 axes
qplot(x=0, y=0, z=0, geom="blank") + 
  theme_void() +
  axes_3D()

## Axes can be populated with points using the function stat_3D.

data(iris)
ggplot(iris, aes(x=Petal.Width, y=Sepal.Width, z=Petal.Length, color=Species)) + 
  theme_void() +
  axes_3D() +
  stat_3D()
```

## References

[^1] http://www.sthda.com/english/wiki/impressive-package-for-3d-and-4d-graph-r-software-and-data-visualization

[^2] https://plot.ly/r/getting-started/

[^3] https://github.com/AckerDWM/gg3D

[^4] https://stackoverflow.com/questions/45052188/how-to-plot-3d-scatter-diagram-using-ggplot
