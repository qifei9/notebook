# LaTeX

## Colors

Add color to text:

``` latex
{\color{declared-color}some text}
```

Add background color to text:

``` latex
\colorbox{declared-color}{text}
```

## Caption

- change caption

``` latex
% change 'Figure' to 'Supplementary Figure'
\renewcommand{\figurename}{Supplementary Figure}
% change '1' to 'S1.'
\renewcommand{\thefigure}{S\arabic{figure}.}
```
