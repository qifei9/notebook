# Analysis

## RNA-seq

### Processing sequencing result

#### bcbio-nextgen

- [Repo](https://github.com/bcbio/bcbio-nextgen)
- [Doc](https://bcbio-nextgen.readthedocs.io/en/latest/contents/introduction.html)

### Exploratory analysis and visualization 

#### Transformations of the counts

In DESeq2:

- `vst()` is much faster than `rlog()`.[^1]
- The authors suggest to use `rlog()` for small dataset (n < 30) and `vst()` for large dataset (n > 30).[^1]
- The authors suggest to use `blind = FALSE` if the transformed counts are use for downstream analysis and visualization.[^2]
- `blind = TRUE` is used for a fully *unsupervised* transformation, for example to perform sample QA[^2] (perhaps also for building machine learning model?).

---

## References

[^1] https://master.bioconductor.org/packages/release/workflows/vignettes/rnaseqGene/inst/doc/rnaseqGene.html

[^2] https://www.bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html
