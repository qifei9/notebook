# Delete commits from git repo

## Using `git reset`

``` bash
git reset <commit-before-the-commit-to-remove>

git push --force
```

## Using `git rebase`

``` bash
git rebase -i <commit-before-the-commit-to-remove>

# delete the line `pick <commit-to-remove>` in the editor

git push --force
```

---

## References

[^1] https://segmentfault.com/q/1010000000115900

[^2] https://blog.csdn.net/QQxiaoqiang1573/article/details/68074847
