# regex

## Modules

- `re`

    ``` python
    import re
    ```

- `regex` (need to install)

    ``` python
    import regex
    ```

## Regular expression

``` python
s = r'ABC\-001' # Python的字符串
# 对应的正则表达式字符串不变：
# 'ABC\-001'
```

``` python
# compile a regular expression for later use

# compile:
>>> re_telephone = re.compile(r'^(\d{3})-(\d{3,8})$')
# use:
>>> re_telephone.match('010-12345').groups()
('010', '12345')
>>> re_telephone.match('010-8086').groups()
('010', '8086')
```

### Named capture

`(?P<foo>Sally)` will capture "Sally" and name it "foo".

### Approximate “fuzzy” matching

Allow some errors when matching.

- `foo` match “foo” exactly
- `(?:foo){i}` match “foo”, permitting insertions
- `(?:foo){d}` match “foo”, permitting deletions
- `(?:foo){s}` match “foo”, permitting substitutions
- `(?:foo){i,s}` match “foo”, permitting insertions and substitutions
- `(?:foo){e}` match “foo”, permitting errors

- `{d<=3}` permit at most 3 deletions, but no other types
- `{i<=1,s<=2}` permit at most 1 insertion and at most 2 substitutions, but no deletions
- `{1<=e<=3}` permit at least 1 and at most 3 errors
- `{i<=2,d<=2,e<=3}` permit at most 2 insertions, at most 2 deletions, at most 3 errors in total, but no substitutions
- etc.[^2]

## Matching

`re.match`

``` python
>>> re.match(r'^\d{3}\-\d{3,8}$', '010-12345')
<_sre.SRE_Match object; span=(0, 9), match='010-12345'>
>>> re.match(r'^\d{3}\-\d{3,8}$', '010 12345')
>>>
```

``` python
test = '用户输入的字符串'
if re.match(r'正则表达式', test):
    print('ok')
else:
    print('failed')
```

## Splitting

`re.split`

``` python
>>> re.split(r'\s+', 'a b   c')
['a', 'b', 'c']
```

## Grouping

``` python
>>> m = re.match(r'^(\d{3})-(\d{3,8})$', '010-12345')
>>> m
<_sre.SRE_Match object; span=(0, 9), match='010-12345'>
>>> m.group(0)
'010-12345'
>>> m.group(1)
'010'
>>> m.group(2)
'12345'
```

## Testing

Regex could be tested on https://regex101.com/

---

## References

[^1] https://www.liaoxuefeng.com/wiki/1016959663602400/1017639890281664

[^2] https://bitbucket.org/mrabarnett/mrab-regex/src/hg/README.rst
