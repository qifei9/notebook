# Odds ratio

## Defination

Odds ratio is the ratio of 2 odds. A [video](https://www.youtube.com/watch?v=JmuciUfCJ_w) explains it clearly.

## Calculation

### R

In R, `fisher.test` calculates the odds ratio of a 2x2 table, and also the *p*-value. The odds ratio and *p*-value could be accessed by `estimate` and `p.value` in the result, respectively.[^1]

**Note**, the odds ratio calculated by `fisher.test` in R is different from that calculated by formula. This is because in `fisher.test` the conditional Maximum Likelihood Estimate (MLE) rather than the unconditional MLE (the sample odds ratio) is used.[^2]

``` R
fisher.test(matrix(x,nr=2))$estimate

fisher.test(matrix(x,nr=2))$p.value
```

### Python

In python, odds ratio could be calculated using `scipy` package.[^3]

``` python
import scipy.stats as stats

table = df.groupby(level="Cancer").sum().values
print(table)

>>> array([[  840, 51663],
           [   32,  5053]])

oddsratio, pvalue = stats.fisher_exact(table)
print("OddsR: ", oddsratio, "p-Value:", pvalue)

>>> OddsR:  2.56743220487 p-Value: 2.72418938361e-09
```

---

## References

[^1] https://stackoverflow.com/questions/41960231/how-to-calculate-multiple-odds-ratios-using-r

[^2] https://stats.stackexchange.com/questions/54530/why-do-odds-ratios-from-formula-and-rs-fisher-test-differ-which-one-should-one/225870

[^3] https://stackoverflow.com/questions/43261747/a-better-way-to-calculate-odd-ratio-in-pandas
