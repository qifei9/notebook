# Read .mat file from MATLAB

Use `readMat()` from `R.matlab`.

``` R
mat <- readMat("t.mat")

names(mat)

mat$<name>
```

The `mat` is a list of lists of lists. For example, if in `t.mat` there are 3 columns and 4 rows, then `mat` contains 3 lists, each for one columns, and every list in `mat` contains 4 lists each for a row in that column.
