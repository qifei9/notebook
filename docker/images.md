# Images

## Location

The default location to store images is `/var/lib/docker`.

## Remove

``` bash
docker rmi <repo>/<image>:<tag>
```
