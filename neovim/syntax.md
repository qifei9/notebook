# Syntax highlight

## Rmarkdown

The original highlight for Rmarkdown in neovim could not highlight LaTex code. Therefore, [vim-pandoc-syntax](https://github.com/vim-pandoc/vim-pandoc-syntax) is preferred.

In `~/.config/nvim/init.vim`, add:

``` vim
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

let g:pandoc#syntax#codeblocks#embeds#langs = [
\   "r",
\   "python",
\   "bash=sh",
\   "shell=sh",
\   "sh",
\   "vim",
\   "help"
\]
```

In `~/.local/share/nvim/plugged/vim-pandoc-syntax/syntax/pandoc.vim`, add:

``` vim
PandocHighlight r
" rmarkdown recognizes embedded R differently than regular pandoc
exe 'syn region pandocRChunk '. 
            \'start=/\(```\s*{\s*r.*\n\)\@<=\_^/ ' .
            \'end=/\_$\n\(\(\s\{4,}\)\=\(`\{3,}`*\|\~\{3,}\~*\)\_$\n\_$\)\@=/ '. 
            \'contained containedin=pandocDelimitedCodeblock contains=@R'

syn region pandocInlineR matchgroup=Operator start=/`r\s/ end=/`/ contains=@R concealends

PandocHighlight python
" rmarkdown recognizes embedded python differently than regular pandoc
exe 'syn region pandocPyChunk '. 
            \'start=/\(```\s*{\s*python.*\n\)\@<=\_^/ ' .
            \'end=/\_$\n\(\(\s\{4,}\)\=\(`\{3,}`*\|\~\{3,}\~*\)\_$\n\_$\)\@=/ '. 
            \'contained containedin=pandocDelimitedCodeblock contains=@PYTHON'

syn region pandocInlinePy matchgroup=Operator start=/`python\s/ end=/`/ contains=@PYTHON concealends
```

to highlight code chunks and in line code of R and python in Rmarkdown.

This code is taken from [vim-rmarkdown](https://github.com/vim-pandoc/vim-rmarkdown) which add support for R code. However, `vim-rmarkdown` conflict with `Nvim-R`.
