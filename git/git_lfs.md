# Git LFS

Track files:

``` bash
git lfs track "design-resources/design.psd"

git add .gitattributes
git add design-resources/design.psd

git commit -m "Add design file"
```

``` bash
git lfs track "*.indd"
git lfs track "design-assets/*"
```

List tracked files:

``` bash
git lfs ls-files
```

**Note:** Git LFS will create `post-checkout`, `post-commit`, `post-merge` and `pre-push` in `.git/hooks`, and these files must have execute permission. If the repo locates in a NTFS partition, the file permission will be a problem.
