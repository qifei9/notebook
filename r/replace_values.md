# Replace values

## Replace

``` R
replace(x, list_of_position, values)
```

Replace by position.

``` R
> replace(
  letters, 
  c(1, 5, 9, 15, 21), 
  c("A", "E", "I", "O", "U")
)
 [1] "A" "b" "c" "d" "E" "f" "g" "h" "I" "j" "k" "l" "m" "n" "O" "p" "q" "r" "s"
[20] "t" "U" "v" "w" "x" "y" "z"
```

## Recode

Could replace by value.

``` R
# For character values, recode values with named arguments only. Unmatched
# values are unchanged.
char_vec <- sample(c("a", "b", "c"), 10, replace = TRUE)
recode(char_vec, a = "Apple")
#>  [1] "Apple" "b"     "b"     "Apple" "Apple" "c"     "c"     "Apple" "b"    
#> [10] "b"    
recode(char_vec, a = "Apple", b = "Banana")
#>  [1] "Apple"  "Banana" "Banana" "Apple"  "Apple"  "c"      "c"      "Apple" 
#>  [9] "Banana" "Banana"
```
