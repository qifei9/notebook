# Dataframe and tibble

## Remove dupicated rows from dataframe

Use `unique()`:[^1]

``` r
a <- c(rep("A", 3), rep("B", 3), rep("C",2))
b <- c(1,1,2,4,1,1,2,2)
df <- data.frame(a,b)
unique(df)

> unique(df)
  a b
1 A 1
3 A 2
4 B 4
5 B 1
7 C 2
```

## Extract a column from a tibble as a vector

Use `pull()` from `dplyr`:[^2]

``` R
col1 <- pull(n, 1)
```

## Split df based on levels of a factor into a list

Use `split()`:[^3]

``` R
X <- split(df, df$g)
str(X)
```

## Add row number as a column

``` r
iris %>%
  mutate(row_name = row_number()) 
```

---

## References

[^1] https://stats.stackexchange.com/questions/6759/removing-duplicated-rows-data-frame-in-r

[^2] https://stackoverflow.com/questions/21618423/extract-a-dplyr-tbl-column-as-a-vector

[^3] https://stackoverflow.com/questions/9713294/split-data-frame-based-on-levels-of-a-factor-into-new-data-frames
