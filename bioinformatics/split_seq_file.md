# Split seq file

## Split by pattern in ids

[^1]

``` bash
seqkit split test.fq -i --id-regexp "\.(\d)\s"
```

---

## References

[^1] https://zhuanlan.zhihu.com/p/46967897
