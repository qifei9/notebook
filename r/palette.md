# Palette

## A colorblind-friendly palette

The palette is from [^1].

``` r
# The palette with grey:
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# The palette with black:
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# To use for fills, add
scale_fill_manual(values=cbPalette)

# To use for line and point colors, add
scale_colour_manual(values=cbPalette)
```

## Emulate ggplot default color palette

Taken from [^2].

``` r
gg_color_hue <- function(n) {
  hues <- seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}

ggcols <- gg_color_hue(2)
```

## Palettes from ColorBrewer

In ggplot2

``` r
scale_colour_brewer(..., type = "seq", palette = 1, direction = 1,
  aesthetics = "colour")

scale_fill_brewer(..., type = "seq", palette = 1, direction = 1,
  aesthetics = "fill")

scale_colour_distiller(..., type = "seq", palette = 1,
  direction = -1, values = NULL, space = "Lab",
  na.value = "grey50", guide = "colourbar", aesthetics = "colour")

scale_fill_distiller(..., type = "seq", palette = 1, direction = -1,
  values = NULL, space = "Lab", na.value = "grey50",
  guide = "colourbar", aesthetics = "fill")
```

## Palettes from viridis

In ggplot2

``` r
scale_colour_viridis_d(..., alpha = 1, begin = 0, end = 1,
  direction = 1, option = "D", aesthetics = "colour")

scale_fill_viridis_d(..., alpha = 1, begin = 0, end = 1,
  direction = 1, option = "D", aesthetics = "fill")

scale_colour_viridis_c(..., alpha = 1, begin = 0, end = 1,
  direction = 1, option = "D", values = NULL, space = "Lab",
  na.value = "grey50", guide = "colourbar", aesthetics = "colour")

scale_fill_viridis_c(..., alpha = 1, begin = 0, end = 1,
  direction = 1, option = "D", values = NULL, space = "Lab",
  na.value = "grey50", guide = "colourbar", aesthetics = "fill")
```

---

## References

[^1] https://jfly.uni-koeln.de/color/

[^2] https://stackoverflow.com/a/8197703
